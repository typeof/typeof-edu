/*

Usage: node jsonToSql.js ./data.json type(1|2|3|4) category_uuid

*/

const pgPromise = require('pg-promise');
const { slugify } = require('transliteration');
const pgp = pgPromise();
const json = require(process.argv[2]);

const connection = {
  host: '95.213.252.55',
  port: 5432,
  database: 'edu',
  user: 'dev',
  password: 'dev'
};

const db = pgp(connection);

const resJson = json.map(article => {
  let start = null;
  if (article.start_date) {
    start = article.start_date.split('.');
    start = new Date(`20${start[2]}`,start[1],start[0])
  }
  let end = null;
  if (article.end_date) {
    end = article.end_date.split('.');
    end = new Date(`20${end[2]}`,end[1],end[0])
  }
  return {
    // platform: article.platform,
    // source: article.source,
    title: article.title,
    description: article.description,
    // full_description: article.full_description,
    tags: article.tags.split(',').map(el => el.trim()),
    media: article.media,
    // lang: article.lang,
    authors: article.authors.split('\n').map(el => el.trim()),
    // authors_description: article.authors_description.split('\n').map(el => el.trim()),
    // authors_media: article.authors_media.split('\n').map(el => el.trim()),
    // duration: article.duration,
    // structure: article.structure.split('\n').map(el => el.trim()),
    // start_date: start,
    // end_date: end,
    ozon: article.ozon,
    // media_video: article.media_video,
    type: process.argv[3],
    alias: slugify(article.title),
  }
});

db.many(pgp.helpers.insert(
  resJson,
  [
    // 'platform',
    // 'source',
    'title',
    'description',
    // 'full_description',
    'tags',
    'media',
    // 'lang',
    'authors',
    // 'authors_description',
    // 'authors_media',
    // 'duration',
    // 'structure',
    // 'start_date',
    // 'end_date',
    // 'cost',
    // 'media_video',
    // 'duration',
    'ozon',
    'alias',
    'type',
  ],
  'materials'
) + ' returning id').then((res) => {
  console.log('res');
  console.log(res);


  const values = res.map(el => {
    return {
      category_id: process.argv[4],
      material_id: el.id,
    }
  })

  db.many(pgp.helpers.insert(
    values,
    [
      'category_id',
      'material_id'
    ],
    'category_materials'
  )).then(el => {
    console.log('res');
    console.log(el);
  }).catch(er => {
    console.log('er');
    console.log(er);
  })

}).catch((err) => {
  console.log('err');
  console.log(err);
});
