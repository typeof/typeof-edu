var ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');

module.exports = {
  plugins: [
    new ScriptExtHtmlWebpackPlugin({
      async: [/client.*js/],
      defaultAttribute: 'sync'
    })
  ]
};
