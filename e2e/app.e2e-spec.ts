import { TypeofEduPage } from './app.po';

describe('typeof-edu App', () => {
  let page: TypeofEduPage;

  beforeEach(() => {
    page = new TypeofEduPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
