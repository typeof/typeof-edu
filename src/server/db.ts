import { IMain, IDatabase } from 'pg-promise';
import * as pgPromise       from 'pg-promise';

export const pgp: IMain = pgPromise();
const connection = {
  host: '95.213.252.55',
  port: 5432,
  database: 'edu',
  user: 'dev',
  password: 'dev'
};

export const db: IDatabase<any> = pgp(connection);

/*

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE categories
(
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY NOT NULL,
    title VARCHAR(30),
    media VARCHAR(100),
    description VARCHAR(500),
    alias VARCHAR(50),
    type VARCHAR(15)
);
CREATE TABLE materials
(
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY NOT NULL,
    title VARCHAR(50),
    type VARCHAR(20),
    media VARCHAR(250),
    duration INTEGER,
    description VARCHAR(500),
    alias VARCHAR(100)
);
CREATE TABLE category_materials
(
    category_id UUID NOT NULL,
    material_id UUID NOT NULL,
    CONSTRAINT category_materials_category_id_material_id_pk PRIMARY KEY (category_id, material_id),
    CONSTRAINT category_materials_categories_id_fk FOREIGN KEY (category_id) REFERENCES categories (id),
    CONSTRAINT category_materials_materials_id_fk FOREIGN KEY (material_id) REFERENCES materials (id)
);
CREATE TABLE material_properties
(
    material_id UUID,
    prop_name VARCHAR(30),
    prop_value VARCHAR(500),
    CONSTRAINT material_properties_materials_id_fk FOREIGN KEY (material_id) REFERENCES materials (id)
);
CREATE TABLE tracks
(
    user_id VARCHAR(50) NOT NULL,
    material_id UUID NOT NULL,
    volume SMALLINT,
    comment VARCHAR(140),
    date DATE NOT NULL,
    CONSTRAINT tracks_pkey PRIMARY KEY (user_id, material_id, date)
);
CREATE TABLE user_materials
(
    user_id VARCHAR(50) NOT NULL,
    material_id UUID NOT NULL,
    date DATE,
    type SMALLINT,
    CONSTRAINT user_materials_pkey PRIMARY KEY (user_id, material_id),
    CONSTRAINT user_materials_materials_id_fk FOREIGN KEY (material_id) REFERENCES materials (id)
);

*/
