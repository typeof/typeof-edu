import { Router, Response, Request } from 'express';
import * as _jwt from 'express-jwt';
import { Converter } from 'showdown';

import { db, pgp } from '../db';

import * as materialsQueries from './sql/materials';

import { Status } from '../../app/shared/index';
import { Category, Material, CourseAuthor } from '../../app/shared/';

import { adminAccess } from './shared/admin-access';

import {
  catchErrorStatus,
  proceedSuccesStatus,
  proceedArrayStatus,
  proceedElementStatus,
} from './shared/status-helpers';

import { authCheck } from './shared/auth-check';

const mdConverter = new Converter();

const materialsRouter: Router = Router();

materialsRouter.post('/changeStatus', authCheck, (request: Request, response: Response) => {

  if (!request.user || !request.user.sub) {
    return catchErrorStatus(response)('Authorization required');
  }

  if (!(
    request.body &&
    request.body.material_id &&
    request.body.status
  )) {
    return catchErrorStatus(response)('Error reading material ID or new status');
  }

  db.one(materialsQueries.changeByUser.changeStatus(request))
    .then(proceedSuccesStatus(response))
    .catch(catchErrorStatus(response));
});

materialsRouter.post('/addAttachement', authCheck, (request: Request, response: Response) => {

  if (!request.user || !request.user.sub) {
    return catchErrorStatus(response)('Authorization required');
  }

  if (!request.body || !request.body.material_id_1) {
    return catchErrorStatus(response)('Error reading material_id_1');
  }

  if (!request.body || !request.body.material_id_2) {
    return catchErrorStatus(response)('Error reading material_id_2');
  }

  adminAccess(request.user.sub).then(() =>
      db.none(materialsQueries.addMaterial.addAttachement(request))
        .then(proceedSuccesStatus(response))
        .catch(catchErrorStatus(response))
    ).catch(() => catchErrorStatus(response)('admin access required'));
});

materialsRouter.post('/removeAttachement', authCheck, (request: Request, response: Response) => {

  if (!request.user || !request.user.sub) {
    return catchErrorStatus(response)('Authorization required');
  }

  if (!request.body || !request.body.material_id_1) {
    return catchErrorStatus(response)('Error reading material_id_1');
  }

  if (!request.body || !request.body.material_id_2) {
    return catchErrorStatus(response)('Error reading material_id_2');
  }

  adminAccess(request.user.sub).then(() => 
    db.none(materialsQueries.addMaterial.removeAttachement(request))
      .then(proceedSuccesStatus(response))
      .catch(catchErrorStatus(response))
  ).catch(() => catchErrorStatus(response)('admin access required'));
});

materialsRouter.get('/isAdmin', authCheck, (request: Request, response: Response) => {

  if (!request.user || !request.user.sub) {
    return response.json(false);
  }

  return adminAccess(request.user.sub).then(value => {
    response.json(true);
  }).catch(() => {
    response.json(false);
  });
});

materialsRouter.post('/addToUser', authCheck, (request: Request, response: Response) => {

  if (!request.user || !request.user.sub) {
    return catchErrorStatus(response)('Authorization required');
  }

  if (!request.body || !request.body.material_id) {
    return catchErrorStatus(response)('Error reading material ID');
  }

  db.one(materialsQueries.changeByUser.addToUser(request))
    .then(proceedSuccesStatus(response))
    .catch(catchErrorStatus(response));
});

materialsRouter.post('/removeFromUser', authCheck, (request: Request, response: Response) => {

  if (!request.user || !request.user.sub) {
    return catchErrorStatus(response)('Authorization required');
  }

  if (!request.body || !request.body.material_id) {
    return catchErrorStatus(response)('Error reading material ID');
  }

  db.one(materialsQueries.changeByUser.removeFromUser(request))
    .then(proceedSuccesStatus(response))
    .catch(catchErrorStatus(response));
});

materialsRouter.post('/', authCheck, (request: Request, response: Response) => {

  if (!request.user) {
    return catchErrorStatus(response)('Authorization required');
  }

  adminAccess(request.user.sub).then(() =>
    db.tx(transaction => {
      return transaction.one(materialsQueries.addMaterial.getQuery(request))
        .then(data => {
          const queries = materialsQueries.addMaterial.getAssignedQueries(request, transaction, data);

          if (queries.length) {
            return transaction.batch(queries);
          } else {
            return Promise.resolve(data);
          }
        });
    })
    .then(proceedSuccesStatus(response))
    .catch(catchErrorStatus(response))
  ).catch(() => catchErrorStatus(response)('admin access required'));
});

const parseMaterialAuthors = (material: Material): CourseAuthor[] =>
  material.authors
  .filter(el => !!el)
  .map((name, ind) => ({
    name,
    description: material.authors_description && material.authors_description[ind] || '',
    media: material.authors_media && material.authors_media[ind] || '',
  }));

materialsRouter.get('/getByAlias/:alias', authCheck, (request: Request, response: Response) => {
  db.none(pgp.as.format(`
    update materials
    set rating = rating + 1
    where alias = $1
  `, [request.params.alias]));

  db.one(materialsQueries.getByAlias.getQuery(request.user, request.params.alias))
  // db.one(materialsQueries.getByAliasAuthed, [request.params.alias, request.user.sub])
    .then((material: Material) => {
      material = new Material(material);
      if (material && material.course && material.authors && material.authors.length) {
        material.courseAuthors = parseMaterialAuthors(material);
      }

      if (material && material.description) {
        material.description = mdConverter.makeHtml(material.description);
      }

      return proceedElementStatus(response, 'материал не найдена')(material);
    })
    .catch(catchErrorStatus(response));
});

materialsRouter.get('/getByCategoryId/:categoryId/:offset/:limit/:sortByRating/:byPlatform', authCheck, (request: Request, response: Response) => {
  db.query(materialsQueries.getByCategory.getQuery(request.user, request.params))
    .then((materials: Material[]) => {
      return proceedArrayStatus(response, 'материалы не найдены')(materials);
    })
    // .then(proceedArrayStatus(response, 'материалы не найдены'))
    .catch(catchErrorStatus(response));
});

materialsRouter.get('/getByUser', authCheck, (request: Request, response: Response) => {
  if (!request.user || !request.user.sub) {
    return catchErrorStatus(response)('Authorization required');
  }

  db.query(materialsQueries.getByUser.getQuery(request.user.sub))
    .then(proceedArrayStatus(response, 'материалы пользователя не найдены'))
    .catch(catchErrorStatus(response));
});

materialsRouter.get('/getByUserSection/:alias', authCheck, (request: Request, response: Response) => {
  if (!request.user || !request.user.sub) {
    return catchErrorStatus(response)('Authorization required');
  }

  db.query(materialsQueries.getByUserSection.getQuery(request))
    .then(proceedArrayStatus(response, 'материалы раздела не найдены'))
    .catch(catchErrorStatus(response));
});

materialsRouter.get('/getByUserStatus/:status', authCheck, (request: Request, response: Response) => {
  if (!request.user || !request.user.sub) {
    return catchErrorStatus(response)('Authorization required');
  }

  db.query(materialsQueries.getByUserStatus.getQuery(request.user.sub, request.params.status))
    .then(proceedArrayStatus(response, 'материалы пользователя не найдены'))
    .catch(catchErrorStatus(response));
});

materialsRouter.get('/', (request: Request, response: Response) => {
  db.query(materialsQueries.getAll.getQuery())
    .then(proceedArrayStatus(response, 'Материалы не найдены'))
    .catch(catchErrorStatus(response));
});


export { materialsRouter };
