import { Router, Response, Request } from 'express';

import { db, pgp } from '../db';
import * as collectionsQueries from './sql/collections';
import { Status } from '../../app/shared/index';
import { Category } from '../../app/shared/';
import { adminAccess } from './shared/admin-access';

import {
  catchErrorStatus,
  proceedSuccesStatus,
  proceedArrayStatus,
  proceedElementStatus,
} from './shared/status-helpers';

import { authCheck } from './shared/auth-check';

const collectionsRouter: Router = Router();

collectionsRouter.post('/', authCheck, (request: Request, response: Response) => {

  if (!request.user || !request.user.sub) {
    return catchErrorStatus(response)('Authorization required');
  }

  adminAccess(request.user.sub).then(() => {
    try {
      db.one(collectionsQueries.addCollection.getQuery(request))
        .then(proceedSuccesStatus(response))
        .catch(catchErrorStatus(response));
    } catch (error) {
      catchErrorStatus(response)(error);
    }
  }).catch(() => catchErrorStatus(response)('admin access required'));
});

collectionsRouter.post('/addCollectionBlock', authCheck, (request: Request, response: Response) => {

  if (!request.user || !request.user.sub) {
    return catchErrorStatus(response)('Authorization required');
  }

  adminAccess(request.user.sub).then(() => {
    try {
      db.query(collectionsQueries.addCollection.addCollectionBlock(request))
        .then(proceedSuccesStatus(response))
        .catch(catchErrorStatus(response));
    } catch (error) {
      catchErrorStatus(response)(error);
    }
  }).catch(() => catchErrorStatus(response)('admin access required'));
});

collectionsRouter.get('/getByAlias/:alias', authCheck, (request: Request, response: Response) => {
  db.one(collectionsQueries.getByAlias.getQuery(request.params.alias, request.user))
    .then(proceedElementStatus(response, 'Подборка не найдена'))
    .catch(catchErrorStatus(response));
});

collectionsRouter.get('/', (request: Request, response: Response) => {
  db.query(collectionsQueries.getAll.getQuery())
    .then(proceedArrayStatus(response, 'Подборки не найдены'))
    .catch(catchErrorStatus(response));
});

export { collectionsRouter };
