import { Router, Response, Request } from 'express';

import { db, pgp } from '../db';
import * as categoriesQueries from './sql/categories';

import { Status } from '../../app/shared/index';
import { authCheck } from './shared/auth-check';

import {
  catchErrorStatus,
  proceedSuccesStatus,
  proceedArrayStatus,
  proceedElementStatus,
} from './shared/status-helpers';
import { Category } from '../../app/shared/';
import { adminAccess } from './shared/admin-access';
const categoriesRouter: Router = Router();

categoriesRouter.post('/', authCheck, (request: Request, response: Response) => {

  if (!request.user || !request.user.sub) {
    return catchErrorStatus(response)('Authorization required');
  }
  
  adminAccess(request.user.sub).then(() => {
    try {
      db.one(categoriesQueries.addCategory.getQuery(request))
        .then(proceedSuccesStatus(response))
        .catch(catchErrorStatus(response));
    } catch (error) {
      catchErrorStatus(response)(error);
    }
  }).catch(() => catchErrorStatus(response)('admin access required'));
});

categoriesRouter.get('/getByAlias/:alias', (request: Request, response: Response) => {
  db.one(categoriesQueries.getByAlias.getQuery(request.params.alias, false))
    .then(proceedElementStatus(response, 'Категория не найдена'))
    .catch(catchErrorStatus(response));
});

categoriesRouter.get('/getByAlias/byPlatform/:alias', (request: Request, response: Response) => {
  db.one(categoriesQueries.getByAlias.getQuery(request.params.alias, true))
    .then(proceedElementStatus(response, 'Категория не найдена'))
    .catch(catchErrorStatus(response));
});

categoriesRouter.get('/', (request: Request, response: Response) => {
  db.query(categoriesQueries.getAll.getQuery(false))
    .then(proceedArrayStatus(response, 'Категории не найдены'))
    .catch(catchErrorStatus(response));
});

categoriesRouter.get('/byPlatform', (request: Request, response: Response) => {
  db.query(categoriesQueries.getAll.getQuery(true))
    .then(proceedArrayStatus(response, 'Категории не найдены'))
    .catch(catchErrorStatus(response));
});

export { categoriesRouter };
