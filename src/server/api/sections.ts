import { Router, Response, Request } from 'express';

import { db, pgp } from '../db';
import * as sectionsQueries from './sql/sections';
import {
  Status,
  Section,
} from '../../app/shared';

import {
  catchErrorStatus,
  proceedSuccesStatus,
  proceedArrayStatus,
  proceedElementStatus,
} from './shared/status-helpers';

import { authCheck } from './shared/auth-check';

const sectionsRouter: Router = Router();

sectionsRouter.post('/', authCheck, (request: Request, response: Response) => {

  if (!request.user || !request.user.sub) {
    return catchErrorStatus(response)('Authorization required');
  }

  if (!request.body) {
    return catchErrorStatus(response)('Error new section reading');
  }

  try {
    db.one(sectionsQueries.addSection.getQuery(request))
      .then(proceedSuccesStatus(response))
      .catch(catchErrorStatus(response));
  } catch (error) {
    catchErrorStatus(response)(error);
  }
});

sectionsRouter.post('/addMaterial', authCheck, (request: Request, response: Response) => {

  if (!request.user || !request.user.sub) {
    return catchErrorStatus(response)('Authorization required');
  }

  if (!(
    request.body &&
    request.body.material_id &&
    request.body.section_id
  )) {
    return catchErrorStatus(response)('Error section or material id reading');
  }

  db.one(sectionsQueries.addMaterial.getQuery(request))
    .then(proceedSuccesStatus(response))
    .catch(catchErrorStatus(response));
});

sectionsRouter.get('/getUserSections', authCheck, (request: Request, response: Response) => {
  if (!request.user || !request.user.sub) {
    return catchErrorStatus(response)('Authorization required');
  }

  db.query(sectionsQueries.getUserSections.getQuery(request.user.sub))
    .then(proceedArrayStatus(response, 'разделы пользователя не найдены'))
    .catch(catchErrorStatus(response));
});

sectionsRouter.get('/getGeneratedSections', authCheck, (request: Request, response: Response) => {
  if (!request.user || !request.user.sub) {
    return catchErrorStatus(response)('Authorization required');
  }

  db.query(sectionsQueries.getGeneratedSections.getQuery(request.user.sub))
    .then(proceedArrayStatus(response, 'Неудалось сформировать разделы'))
    .catch(catchErrorStatus(response));
});

sectionsRouter.get('/getByAlias/:alias', authCheck, (request: Request, response: Response) => {
  if (!request.user || !request.user.sub) {
    return catchErrorStatus(response)('Authorization required');
  }

  db.one(sectionsQueries.getByAlias.getQuery(request))
    .then(proceedElementStatus(response, 'Раздел не найдена'))
    .catch(catchErrorStatus(response));
});

export { sectionsRouter };
