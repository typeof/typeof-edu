import { db, pgp } from '../../db';

export const adminAccess =(user_id: string) => {

  return db.one(
      pgp.as.format(`
        select admin
        from admins
        where
          user_id = $1 and
          admin = true
      `, [user_id])
    );
};
