import { Response } from '@types/express-serve-static-core/index';
import { Status } from '../../../app/shared/index';

export const catchErrorStatus = (response: Response) => (error) => {
  return response.json({
    success: false,
    msg: error.message || error,
  } as Status<null>);
};

export const proceedSuccesStatus = (response: Response) => () => {
  return response.json({
    success: true,
    msg: '',
  } as Status<null>);
};

export const proceedArrayStatus = (response: Response, err: string) => (items: Object[]) => {
  return response.json({
    success: items && !!items.length,
    msg:     items && !!items.length ? '' : err,
    data:    items || [],
  } as Status<Object[]>);
};

export const proceedElementStatus = (response: Response, err: string) => (item: Object) => {
  return response.json({
    success: !!item,
    msg:     !!item ? '' : err,
    data:    item || {},
  } as Status<Object>);
};
