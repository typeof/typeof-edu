import { Response, Request } from 'express';

import * as jwt from 'jsonwebtoken';

const jwtSecret = 'LUh6B9qnKdiylhSe9HoG23G90D4wjJRkUfpgDwVPEce__QJ-Zk7cC2fZXEKq2iFk';
const jwtOptions = { audience: 'wYoEnzt1QnChuh0iEGCn22JYuFHCqyfd' };

export const authCheck = (req: Request & { headers: { authorization: string } }, response: Response, next) => {
  if (req.headers && req.headers.authorization) {
    // console.log(req.headers.authorization);
    const parts = req.headers.authorization.split(' ');
    if (parts.length == 2) {
      const scheme = parts[0];
      const credentials = parts[1];

      if (/^Bearer$/i.test(scheme)) {
        jwt.verify(credentials, jwtSecret, jwtOptions, (err, decoded) => {
          if (!err && decoded) {
            req.user = decoded
          } else {
            req.user = false;
          }
          next();
        });
      } else {
        req.user = false;
        next();
      }
    } else {
      req.user = false;
      next();
    }
  } else {
    req.user = false;
    next();
  }
}
