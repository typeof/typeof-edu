import { Request } from 'express';
import { db, pgp } from '../../../db';
import { UUID, textArray } from '../shared';

const { slugify } = require('transliteration');

export class addCollection {

  static addCollectionBlock(request: Request) {

    if (!request.body.collection_id) {
      throw new Error('Collection id is not defined');
    }

    const collectionBlock = {
      collection_id: request.body.collection_id,
      materials_ids: new UUID(request.body.materials_ids) || [],
      title: request.body.title || '',
      type: request.body.type || '',
      "order": request.body.order || 0,
      background: request.body.background || '',
      // texts_1: new textArray(request.body.texts_1 || []),
      texts_1: request.body.texts_1 && request.body.texts_1.length ? request.body.texts_1 : new textArray([]),
      texts_2: request.body.texts_2 && request.body.texts_2.length ? request.body.texts_2 : new textArray([]),
      medias: request.body.medias && request.body.medias.length ? request.body.medias : new textArray([]),
    };

    return pgp.helpers.insert(collectionBlock, null, 'collections_blocks');
  }

  static getQuery(request: Request) {

    if (!request.body.title) {
      throw new Error('Title is not defined');
    }

    if (request.body.title.length > 100) {
      throw new Error('Title is too long');
    }

    if (!request.body.media) {
      throw new Error('Media is not defined');
    }

    if (!request.body.tags) {
      throw new Error('Tags is not defined');
    }

    if (!request.body.description) {
      throw new Error('Description is not defined');
    }

    if (!request.body.alias) {
      request.body.alias = slugify(request.body.title);
    }

    const collection = {
      title: request.body.title,
      media: request.body.media,
      alias: request.body.alias,
      tags: request.body.tags,
      description: request.body.description,
    };

    return pgp.helpers.insert(collection, null, 'collections') + ' returning id';
  }
}
