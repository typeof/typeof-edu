import { db, pgp } from '../../../db';

export class getByAlias {

  static getQuery(alias, user) {
    return pgp.as.format(`
      SELECT
        collections.title,
        collections.description,
        collections.tags,
        collections.alias,
        collections.media,
        array_to_json(
          ARRAY(
            SELECT json_build_object(
              'order',
              collections_blocks.order,
              'type',
              collections_blocks.type,
              'title',
              collections_blocks.title,
              'background',
              collections_blocks.background,
              'texts_1',
              collections_blocks.texts_1,
              'texts_2',
              collections_blocks.texts_2,
              'medias',
              collections_blocks.medias,
              'urls',
              collections_blocks.urls,
              'materials_ids',
              collections_blocks.materials_ids,
              'materials',
              array_to_json(
                ARRAY(
                  SELECT json_build_object(
                    'id',
                    materials.id,
                    'title',
                    materials.title,
                    'alias',
                    materials.alias,
                    'media',
                    materials.media,
                    'media_video',
                    materials.media_video,
                    'type',
                    materials.type,
                    'description',
                    materials.description
                    ${user && user.sub ? `
                      ,
                      'added',
                      (
                        SELECT count(*)::int::bool
                        FROM materials as materials_3
                          LEFT OUTER JOIN user_materials ON materials_3.id = user_materials.material_id
                        WHERE
                          materials_3.id = materials.id AND
                          user_materials.user_id = $2
                      )
                    ` : ''}
                    
                  )
                  FROM materials
                  WHERE materials.id = ANY (collections_blocks.materials_ids)
                )
              )
            ) as blocks
            FROM collections_blocks
            WHERE collections.id = collections_blocks.collection_id
            ORDER BY collections_blocks."order"
          )
        ) as blocks
      FROM
        collections
      WHERE collections.alias = $1
    `, [alias, user.sub]);
  }
}
