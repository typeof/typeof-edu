import { db, pgp } from '../../../db';

export class getAll {

  static getQuery() {
    return `
      SELECT
        (
          SELECT count(*)
          FROM
            materials, collection_materials
          WHERE
            materials.type = 1 AND
            collection_materials.collection_id = collections.id AND
            collection_materials.material_id = materials.id
        )::int AS books,
        (
          SELECT count(*)
          FROM
            materials, collection_materials
          WHERE
            materials.type = 3 AND
            collection_materials.collection_id = collections.id AND
            collection_materials.material_id = materials.id
        )::int AS courses,
        (SELECT count(*)
         FROM
           materials, collection_materials
         WHERE
           materials.type = 2 AND
           collection_materials.collection_id = collections.id AND
           collection_materials.material_id = materials.id
        )::int AS videos,
        (SELECT count(*)
         FROM
           materials, collection_materials
         WHERE
           materials.type = 4 AND
           collection_materials.collection_id = collections.id AND
           collection_materials.material_id = materials.id
        )::int AS articles,
        id,
        alias,
        title,
        tags,
        description,
        media
      FROM
        collections
      ORDER BY collections.order
    `;
  }
}
