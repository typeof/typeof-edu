import { Request } from 'express';
import { db, pgp } from '../../../db';
import { Section } from '../../../../app/shared';

const { slugify } = require('transliteration');

export class addSection {

  static getQuery(request: Request) {

    if (!request.body.title) {
      throw new Error('Title is not defined');
    }

    if (request.body.title.length > 50) {
      throw new Error('Title is too long');
    }

    if (!request.body.color) {
      throw new Error('Color is not defined');
    }

    if (!request.user.sub) {
      throw new Error('User id is not defined');
    }

    if (!request.body.alias) {
      request.body.alias = slugify(request.body.title);
    }

    const section: Section = {
      title: request.body.title,
      alias: request.body.alias,
      color: request.body.color,
      user_id: request.user.sub,
    };

    return pgp.helpers.insert(section, null, 'sections') + ' returning id';
  }
}
