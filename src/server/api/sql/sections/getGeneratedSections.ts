import { db, pgp } from '../../../db';

export class getGeneratedSections {
  static getQuery(user) {
    return pgp.as.format(`
      SELECT
        categories.id,
        categories.title,
        categories.alias,
        categories.media,
        sum((
          SELECT count(*)
          FROM
            materials as inner_materials
          WHERE
            materials.type = 1 AND
            inner_materials.id = materials.id
        )) AS books,

        sum((
          SELECT count(*)
          FROM
            materials as inner_materials
          WHERE
            materials.type = 2 AND
            inner_materials.id = materials.id
        )) AS videos,

        sum((
          SELECT count(*)
          FROM
            materials as inner_materials
          WHERE
            materials.type = 3 AND
            inner_materials.id = materials.id
        )) AS courses,

        sum((
          SELECT count(*)
          FROM
            materials as inner_materials
          WHERE
            materials.type = 4 AND
            inner_materials.id = materials.id
        )) AS articles,


        sum((
          SELECT count(*)
          FROM
            user_materials as inner_user_materials
          WHERE
            inner_user_materials.status = 1 AND
            inner_user_materials.user_id = user_materials.user_id AND
            inner_user_materials.material_id = materials.id
        )) AS backlog,

        sum((
          SELECT count(*)
          FROM
            user_materials as inner_user_materials
          WHERE
            inner_user_materials.status = 2 AND
            inner_user_materials.user_id = user_materials.user_id AND
            inner_user_materials.material_id = materials.id
        )) AS "inProgress",

        sum((
          SELECT count(*)
          FROM
            user_materials as inner_user_materials
          WHERE
            inner_user_materials.status = 3 AND
            inner_user_materials.user_id = user_materials.user_id AND
            inner_user_materials.material_id = materials.id
        )) AS done


      FROM materials
        INNER JOIN user_materials ON materials.id = user_materials.material_id
        INNER JOIN category_materials ON materials.id = category_materials.material_id
        INNER JOIN categories ON category_materials.category_id = categories.id

      WHERE user_materials.user_id = $1
      GROUP BY categories.id
    `, [user]);
  }
}
