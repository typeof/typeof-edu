import { Request } from 'express';
import { db, pgp } from '../../../db';

export class addMaterial {

  static getQuery(request: Request) {

    const relation = {
      material_id: request.body.material_id || '',
      section_id: request.body.section_id || '',
    };

    return pgp.helpers.insert(relation, null, 'section_materials');
  }
}
