import { db, pgp } from '../../../db';

export class getUserSections {
  static getQuery(user) {
    return pgp.as.format(`
      SELECT
        id,
        title,
        alias,
        color,

        (
          SELECT count(*)
          FROM
            materials, section_materials
          WHERE
            materials.type = 1 AND
            section_materials.section_id = sections.id AND
            section_materials.material_id = materials.id
        ) AS books,

        (
          SELECT count(*)
          FROM
            materials, section_materials
          WHERE
            materials.type = 2 AND
            section_materials.section_id = sections.id AND
            section_materials.material_id = materials.id
        ) AS videos,

        (
          SELECT count(*)
          FROM
            materials, section_materials
          WHERE
            materials.type = 3 AND
            section_materials.section_id = sections.id AND
            section_materials.material_id = materials.id
        ) AS courses,

        (
          SELECT count(*)
          FROM
            materials, section_materials
          WHERE
            materials.type = 4 AND
            section_materials.section_id = sections.id AND
            section_materials.material_id = materials.id
        ) AS articles,

        (
          SELECT count(*)
          FROM
            user_materials, section_materials
          WHERE
            user_materials.status = 1 AND
            section_materials.material_id = user_materials.material_id AND
            section_materials.section_id = sections.id AND
            user_materials.user_id = $1
        ) AS backlog,

        (
          SELECT count(*)
          FROM
            user_materials, section_materials
          WHERE
            user_materials.status = 2 AND
            section_materials.material_id = user_materials.material_id AND
            section_materials.section_id = sections.id AND
            user_materials.user_id = $1
        ) AS "inProgress",

        (
          SELECT count(*)
          FROM
            user_materials, section_materials
          WHERE
            user_materials.status = 3 AND
            section_materials.material_id = user_materials.material_id AND
            section_materials.section_id = sections.id AND
            user_materials.user_id = $1
        ) AS done

      FROM sections
      WHERE user_id = $1
    `, [user]);
  }
}
