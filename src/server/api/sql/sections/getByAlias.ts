import { Request } from 'express';

import { db, pgp } from '../../../db';

export class getByAlias {
  // TODO: books/videos/etc
  static getQuery(request: Request) {
    return pgp.as.format(`
      SELECT
        *
      FROM
        sections
      WHERE
        user_id = $1 AND
        alias = $2
    `, [request.user.sub, request.params.alias]);
  }
}
