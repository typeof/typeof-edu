import { db, pgp } from '../../../db';

export class getByAlias {

  static getQuery(alias, byPlatform: boolean = false) {
    return pgp.as.format(`
      SELECT
        (
          SELECT count(*)
          FROM
            materials ${byPlatform ? '' : ', category_materials'}
          WHERE
            materials.type = 1 AND
            ${byPlatform ? '' : 'category_materials.category_id = categories.id AND'}
            ${byPlatform ? 'categories.title = materials.platform AND' : 'category_materials.material_id = materials.id AND'}
            materials.show = true
        ) AS books,
        (
          SELECT count(*)
          FROM
            materials ${byPlatform ? '' : ', category_materials'}
          WHERE
            materials.type = 3 AND
            ${byPlatform ? '' : 'category_materials.category_id = categories.id AND'}
            ${byPlatform ? 'categories.title = materials.platform AND' : 'category_materials.material_id = materials.id AND'}
            materials.show = true
        ) AS courses,
        (SELECT count(*)
         FROM
           materials ${byPlatform ? '' : ', category_materials'}
         WHERE
           materials.type = 2 AND
           ${byPlatform ? '' : 'category_materials.category_id = categories.id AND'}
           ${byPlatform ? 'categories.title = materials.platform AND' : 'category_materials.material_id = materials.id AND'}
           materials.show = true
        ) AS videos,
        (SELECT count(*)
         FROM
           materials ${byPlatform ? '' : ', category_materials'}
         WHERE
           materials.type = 4 AND
           ${byPlatform ? '' : 'category_materials.category_id = categories.id AND'}
           ${byPlatform ? 'categories.title = materials.platform AND' : 'category_materials.material_id = materials.id AND'}
            materials.show = true
        ) AS articles,
        id,
        alias,
        title,
        description,
        type,
        communities_name,
        communities_description,
        communities_media,
        communities_source,
        tags,
        media
      FROM
        categories
      WHERE
        alias = $1
    `, [alias]);
  }
}
