import { db, pgp } from '../../../db';

export class getAll {

  static getQuery(byPlatform: boolean = false) {
    return `
      SELECT
        (
          SELECT count(*)
          FROM
            materials ${byPlatform ? '' : ', category_materials'}
          WHERE
            materials.type = 1 AND
            ${byPlatform ? '' : 'category_materials.category_id = categories.id AND'}
            ${byPlatform ? 'categories.title = materials.platform AND' : 'category_materials.material_id = materials.id AND'}
            materials.show = true
        ) AS books,
        (
          SELECT count(*)
          FROM
            materials ${byPlatform ? '' : ', category_materials'}
          WHERE
            materials.type = 3 AND
            ${byPlatform ? '' : 'category_materials.category_id = categories.id AND'}
            ${byPlatform ? 'categories.title = materials.platform AND' : 'category_materials.material_id = materials.id AND'}
            materials.show = true
        ) AS courses,
        (SELECT count(*)
         FROM
           materials ${byPlatform ? '' : ', category_materials'}
         WHERE
           materials.type = 2 AND
           ${byPlatform ? '' : 'category_materials.category_id = categories.id AND'}
           ${byPlatform ? 'categories.title = materials.platform AND' : 'category_materials.material_id = materials.id AND'}
           materials.show = true
        ) AS videos,
        (SELECT count(*)
         FROM
           materials ${byPlatform ? '' : ', category_materials'}
         WHERE
           materials.type = 4 AND
           ${byPlatform ? '' : 'category_materials.category_id = categories.id AND'}
           ${byPlatform ? 'categories.title = materials.platform AND' : 'category_materials.material_id = materials.id AND'}
            materials.show = true
        ) AS articles,
        id,
        alias,
        title,
        description,
        tags,
        type,
        media
      FROM
        categories
      WHERE
        type = ${byPlatform ? '2' : '1'}
    `;
  }
}
