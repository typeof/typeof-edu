import { Request } from 'express';
import { db, pgp } from '../../../db';

const { slugify } = require('transliteration');

export class addCategory {

  static getQuery(request: Request) {

    if (!request.body.title) {
      throw new Error('Title is not defined');
    }

    if (request.body.title.length > 50) {
      throw new Error('Title is too long');
    }

    if (!request.body.alias) {
      request.body.alias = slugify(request.body.title);
    }

    const category = {
      title: request.body.title,
      alias: request.body.alias,
      media: request.body.media || '',
      description: request.body.description || '',
      type: request.body.type || 1,
    };

    return pgp.helpers.insert(category, null, 'categories') + ' returning id';
  }
}
