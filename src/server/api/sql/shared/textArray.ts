import { pgp } from '../../../db';

export function textArray(array) {
  this.array = array;
  this._rawDBType = true; // force raw format on output;
  this.formatDBType = function () {
    this._rawDBType = true; // force raw format on output;
    return pgp.as.format('$1::text[]', [this.array]);
  };
};