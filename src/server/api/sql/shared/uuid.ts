import { pgp } from '../../../db';

export function UUID(uuids) {
  this.uuids = uuids;
  this._rawDBType = true; // force raw format on output;
  this.formatDBType = function () {
    this._rawDBType = true; // force raw format on output;
    return pgp.as.format('$1::uuid[]', [this.uuids]);
  };
}
