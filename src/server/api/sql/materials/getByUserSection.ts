import { Request } from 'express';

import { db, pgp } from '../../../db';

export class getByUserSection {

  static getQuery(request: Request) {
    return pgp.as.format(`
      SELECT
        materials.id,
        materials.alias,
        materials.authors,
        materials.title,
        materials.description,
        materials.type,
        materials.media,
        materials.platform,
        materials.duration,
        true as added,
        (
          SELECT status
          FROM user_materials
            LEFT OUTER JOIN materials ON materials.id = user_materials.material_id
          WHERE
            user_materials.user_id = $2 AND
            materials.id = section_materials.material_id
        ) AS status
      FROM
        section_materials, materials, sections
      WHERE
        section_materials.material_id = materials.id AND
        section_id = sections.id AND
        sections.alias = $1
    `, [request.params.alias, request.user.sub]);
  }
}
