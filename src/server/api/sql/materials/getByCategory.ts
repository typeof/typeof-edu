import { db, pgp } from '../../../db';

export class getByCategory {

  static authed = `
    ,
    (
      SELECT count(*)::int::bool
      FROM materials
        LEFT OUTER JOIN user_materials ON materials.id = user_materials.material_id
      WHERE
        user_materials.user_id = $4 AND
        materials.id = category_materials.material_id
    ) AS added,
    (
      SELECT status
      FROM user_materials
        LEFT OUTER JOIN materials ON materials.id = user_materials.material_id
      WHERE
        user_materials.user_id = $4 AND
        materials.id = category_materials.material_id
    ) AS status
  `;

  static authedByPlatform = `
    ,
    (
      SELECT count(*)::int::bool
      FROM materials as materials_1
        LEFT OUTER JOIN user_materials ON materials.id = user_materials.material_id
      WHERE
        user_materials.user_id = $4 AND
        materials_1.id = materials.id
    ) AS added,
    (
      SELECT status
      FROM user_materials
        LEFT OUTER JOIN materials as materials_1 ON materials.id = user_materials.material_id
      WHERE
        user_materials.user_id = $4 AND
        materials_1.id = materials.id
    ) AS status
  `;

  static getValues(user, params) {
    return (user && user.sub) ?
      [
        params.categoryId,
        params.limit,
        params.offset,
        user.sub
      ] : [
        params.categoryId,
        params.limit,
        params.offset,
      ];
  }

  static getQuery(user, params) {
    return pgp.as.format(`
      select
        id,
        alias,
        authors,
        title,
        description,
        type,
        media,
        tags,
        platform,
        created_at,
        start_date,
        end_date,
        rating,
        duration
        ${!user ? '' : (+params.byPlatform ? this.authedByPlatform : this.authed)}
      from
        materials ${+params.byPlatform ? '' : ', category_materials'}
      where
        ${+params.byPlatform ? 'platform' : 'category_id'} = $1 and
        ${+params.byPlatform ? '' : 'material_id = materials.id and'}
        show = true
      order by ${+params.sortByRating ? 'rating' : 'created_at'} desc
      ${+params.limit ? 'limit $2' : '' }
      offset $3
    `, this.getValues(user, params));
  }
}
