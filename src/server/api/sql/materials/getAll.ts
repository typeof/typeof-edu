import { db, pgp } from '../../../db';

export class getAll {

  static getQuery() {
    return `
      SELECT
        id, alias, title, description, type, media, duration
      FROM
        materials
    `;
  }
}
