import { Request } from 'express';
import { db, pgp } from '../../../db';

import { MaterialStatusType } from '../../../../app/shared';

export class changeByUser {

  static changeStatus(req: Request) {
    return pgp.as.format(`
      UPDATE
        user_materials
      SET
        status = $1
      WHERE
        user_id = $2 AND
        material_id = $3
      returning material_id
    `, [req.body.status, req.user.sub, req.body.material_id]);
  }

  static removeFromUser(req: Request) {
    return pgp.as.format(`
      DELETE FROM
        user_materials
      WHERE
        user_id = $1 AND
        material_id = $2
      returning material_id
    `, [req.user.sub, req.body.material_id]);
  }

  static addToUser(req: Request) {

    const row = {
      user_id: req.user.sub,
      material_id: req.body.material_id,
      date: new Date(),
      status: req.body.status || MaterialStatusType.backlog,
    };

    return pgp.helpers.insert(row, null, 'user_materials') + ' RETURNING material_id';
  }
}
