import { db, pgp } from '../../../db';

export class getByUserStatus {

  static getQuery(user, status) {
    return pgp.as.format(`
      SELECT *, true as added 
      FROM user_materials
        LEFT OUTER JOIN materials
          ON user_materials.material_id = materials.id
      WHERE
        user_id = $1 AND
        status = $2
    `, [user, status]);
  }
}
