import { Request } from 'express';
import { db, pgp } from '../../../db';
import { textArray } from '../shared';

const { slugify } = require('transliteration');

export class addMaterial {

  static getAssignedQueries(request: Request, transaction, data) {
    const queries = [];

    if (data.id &&
      request.body.categories_ids &&
      request.body.categories_ids.length
    ) {
      queries.push(transaction.none(
        addMaterial.assignMaterialWithCategories(
          data.id,
          request.body.categories_ids
        )
      ));
    }

    if (data.id &&
      request.body.collections_ids &&
      request.body.collections_ids.length
    ) {
      queries.push(transaction.none(
        addMaterial.assignMaterialWithCollections(
          data.id,
          request.body.collections_ids
        )
      ));
    }

    if (data.id &&
      request.body.materials_ids &&
      request.body.materials_ids.length
    ) {
      queries.push(transaction.none(
        addMaterial.assignMaterialWithMaterials(
          data.id,
          request.body.materials_ids
        )
      ));
    }

    return queries;
  }

  static assignMaterialWithCategories(id: string, categories: string[]) {
    const values = categories.map(category => ({
      category_id: category,
      material_id: id,
    }));

    return pgp.helpers.insert(values, ['category_id', 'material_id'], 'category_materials');
  };

  static assignMaterialWithCollections(id: string, collections: string[]) {
    const values = collections.map(collection => ({
      collection_id: collection,
      material_id: id,
    }));

    return pgp.helpers.insert(values, ['collection_id', 'material_id'], 'collection_materials');
  };

  static assignMaterialWithMaterials(id: string, materials: string[]) {
    const values = materials.map(material => ({
      material_1: id,
      material_2: material,
    }));

    return pgp.helpers.insert(values, ['material_1', 'material_2'], 'materials_materials');
  };

  static getQuery(request: Request) {

    if (!request.body.title) {
      throw new Error('Title is not defined');
    }

    if (!request.body.media) {
      throw new Error('Media is not defined');
    }

    if (!(0 < request.body.type && request.body.type < 5)) {
      throw new Error('Type not defined or incorrect');
    }

    if (!request.body.alias) {
      request.body.alias = slugify(request.body.title);
    }

    const material = {
      title: request.body.title,
      type: request.body.type,
      media: request.body.media,
      alias: request.body.alias,
      duration: request.body.duration || 0,
      description: request.body.description || '',
      authors: new textArray(request.body.authors || []),
      authors_description: new textArray(request.body.authors_description || []),
      authors_media: new textArray(request.body.authors_media || []),
      tags: new textArray(request.body.tags || []),
      platform: request.body.platform || '',
      lang: request.body.lang || '',
      cost: request.body.cost || 0,
      currency: request.body.currency || '',
      full_description: request.body.full_description || '',
      structure: new textArray(request.body.structure || []),
      start_date: request.body.start_date || null,
      end_date: request.body.end_date || null,
    };

    return pgp.helpers.insert(material, null, 'materials') + ' returning id';
  }

  static addAttachement(request: Request) {
    const row = {
      material_id_1: request.body.material_id_1,
      material_id_2: request.body.material_id_2,
    };

    return pgp.helpers.insert(row, null, 'materials_materials');
  }

  static removeAttachement(request: Request) {

    return pgp.as.format(`
      DELETE FROM
        materials_materials
      WHERE
        material_id_1 = $1 AND
        material_id_2 = $2
    `, [request.body.material_id_1, request.body.material_id_2]);
  }
}
