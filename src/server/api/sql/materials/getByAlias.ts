import { db, pgp } from '../../../db';

export class getByAlias {

  static authed = `
    ,
    (
      SELECT count(*)::int::bool
      FROM materials
        LEFT OUTER JOIN user_materials ON materials.id = user_materials.material_id
      WHERE
        materials.alias = $1 AND
        user_materials.user_id = $2
    ) AS added,
    (
      SELECT status
      FROM user_materials
        LEFT OUTER JOIN materials ON materials.id = user_materials.material_id
      WHERE
        materials.alias = $1 AND
        user_materials.user_id = $2
    ) AS status
  `;
  
  static innerAuthed = `
    ,
    'added',
    (
      SELECT count(*)::int::bool
      FROM materials as materials_3
        LEFT OUTER JOIN user_materials ON materials_3.id = user_materials.material_id
      WHERE
        materials_3.id = materials_2.id AND
        user_materials.user_id = $2
    ),
    'status',
    (
      SELECT status
      FROM user_materials
        LEFT OUTER JOIN materials as materials_3 ON materials_3.id = user_materials.material_id
      WHERE
        materials_3.id = materials_2.id AND
        user_materials.user_id = $2
    )
  `;

  static getValues(user, alias) {
    return user && user.sub ? [alias, user.sub] : [alias];
  }

  static getQuery(user, alias) {
    return pgp.as.format(`
      SELECT
        id,
        alias,
        title,
        description,
        authors,
        authors_description,
        authors_media,
        tags,
        lang,
        cost,
        materials.type,
        structure,
        media,
        media_video,
        platform,
        duration,
        source,
        links,
        links_name,
        rating,
        array_to_json(
          ARRAY(
            SELECT json_build_object(
              'title',
              categories.title,
              'id',
              categories.id,
              'alias',
              categories.alias,
              'description',
              categories.description,
              'tags',
              categories.tags,
              'type',
              categories.type,
              'media',
              categories.media,
              'books',
              (
                SELECT count(*)
                FROM
                  materials, category_materials
                WHERE
                  materials.type = 1 AND
                  category_materials.category_id = categories.id AND
                  category_materials.material_id = materials.id
              ),
              'courses',
              (
                SELECT count(*)
                FROM
                  materials, category_materials
                WHERE
                  materials.type = 3 AND
                  category_materials.category_id = categories.id AND
                  category_materials.material_id = materials.id
              ),
              'videos',
              (SELECT count(*)
               FROM
                 materials, category_materials
               WHERE
                 materials.type = 2 AND
                 category_materials.category_id = categories.id AND
                 category_materials.material_id = materials.id
              ),
              'articles',
              (SELECT count(*)
               FROM
                 materials, category_materials
               WHERE
                 materials.type = 4 AND
                 category_materials.category_id = categories.id AND
                 category_materials.material_id = materials.id
              )
            )
            FROM
              materials
              INNER JOIN category_materials ON materials.id = category_materials.material_id
              INNER JOIN categories ON category_materials.category_id = categories.id
            WHERE
              materials.alias = $1
          )
        ) AS categories,
        array_to_json(
          ARRAY(
            SELECT json_build_object(
              'title',
              collections.title,
              'id',
              collections.id,
              'alias',
              collections.alias,
              'tags',
              collections.tags,
              'description',
              collections.description,
              'media',
              collections.media,
              'books',
              (
                SELECT count(*)
                FROM
                  materials, collection_materials
                WHERE
                  materials.type = 1 AND
                  collection_materials.collection_id = collections.id AND
                  collection_materials.material_id = materials.id
              ),
              'courses',
              (
                SELECT count(*)
                FROM
                  materials, collection_materials
                WHERE
                  materials.type = 3 AND
                  collection_materials.collection_id = collections.id AND
                  collection_materials.material_id = materials.id
              ),
              'videos',
              (SELECT count(*)
               FROM
                 materials, collection_materials
               WHERE
                 materials.type = 2 AND
                 collection_materials.collection_id = collections.id AND
                 collection_materials.material_id = materials.id
              ),
              'articles',
              (SELECT count(*)
               FROM
                 materials, collection_materials
               WHERE
                 materials.type = 4 AND
                 collection_materials.collection_id = collections.id AND
                 collection_materials.material_id = materials.id
              )
            )
            FROM
              materials
              INNER JOIN collection_materials ON materials.id = collection_materials.material_id
              INNER JOIN collections ON collection_materials.collection_id = collections.id
            WHERE
              materials.alias = $1
          )
        ) AS collections,
        array_to_json(
          ARRAY(
            SELECT json_build_object(
              'title',
              materials_2.title,
              'id',
              materials_2.id,
              'alias',
              materials_2.alias,
              'type',
              materials_2.type,
              'description',
              materials_2.description,
              'platform',
              materials_2.platform,
              'media',
              materials_2.media
              ${!user ? '' : this.innerAuthed}
            )
            FROM
              materials
              INNER JOIN materials_materials ON materials.id = materials_materials.material_id_1
              INNER JOIN materials as materials_2 ON materials_materials.material_id_2 = materials_2.id
            WHERE
              materials.alias = $1
          )
        ) AS materials
        ${!user ? '' : this.authed}
      FROM
        materials
      WHERE
        materials.alias = $1
      LIMIT 1
    `, this.getValues(user, alias));
  }
}
