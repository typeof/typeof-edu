import { db, pgp } from '../../../db';

export class getByUser {

  static getQuery(user) {
    return pgp.as.format(`
      SELECT 
        id,
        title,
        media,
        duration,
        description,
        alias,
        authors,
        tags,
        platform,
        lang,
        cost,
        start_date,
        end_date,
        type,
        source,
        created_at,
        rating,
        true as added,
        date,
        status
      FROM user_materials
        LEFT OUTER JOIN materials
          ON user_materials.material_id = materials.id
      WHERE user_id = $1
      order by date desc
    `, [user]);
  }
}
