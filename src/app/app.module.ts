import { UniversalModule } from 'angular2-universal';
import { FormsModule } from '@angular/forms';
import { ModuleWithProviders } from '@angular/core';
// import { RouterStoreModule } from '@ngrx/router-store';
import { MaterialModule } from '@angular/material';

import { AppRoutingModule } from './app-routing.module';

import { UniversityModule } from './university/university.module';
import { HomeModule } from './home/home.module';
import { CoreModule } from './core/core.module';
import { AdminModule } from './admin/admin.module';
import { SharedModule } from './shared/shared.module';

import { SecondNavComponent } from './shared/second-nav/second-nav.component';

export const commonImports = [

  MaterialModule,
  MaterialModule.forRoot(),
  AppRoutingModule,
  HomeModule,
  CoreModule,
  UniversityModule,
  SharedModule,
  AdminModule,
  UniversalModule,
  FormsModule,
];

export const commonDeclarations = [
  SecondNavComponent,
];
