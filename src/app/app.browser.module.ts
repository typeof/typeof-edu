import { NgModule } from '@angular/core';

import { commonImports, commonDeclarations } from './app.module';

import { RouterStoreModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { rootReducer } from './reducers/index';

import { Http, RequestOptions } from '@angular/http';
import { AuthHttp, AuthConfig } from 'angular2-jwt';

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig(), http, options);
}

import { AppComponent } from './index';
import { CacheService } from './shared/index';
import { ClientAuthService } from './shared/auth/client-auth.service';

export function getLRU(lru?: any) {
  return lru || new Map();
}
export function getRequest() {
  return { cookie: document.cookie };
}
export function getResponse() {
  return {};
}

export const UNIVERSAL_KEY = 'UNIVERSAL_CACHE';

/**
 * Top-level NgModule "container"
 */
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    commonDeclarations,
    AppComponent,
  ],
  imports: [
    commonImports,
    RouterStoreModule.connectRouter(),
    StoreModule.provideStore(rootReducer, {
      router: {
        path: window.location.pathname + window.location.search
      }
    }),
  ],
  providers: [
    { provide: 'LRU', useFactory: getLRU, deps: [] },
    { provide: 'req', useFactory: getRequest },
    { provide: 'res', useFactory: getResponse },
    CacheService,
    ClientAuthService,
    {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions]
    },
  ],
})

export class AppModule {
  constructor(public cache: CacheService) {
    // TODO(gdi2290): refactor into a lifecycle hook
    this.doRehydrate();
  }

  doRehydrate() {
    let defaultValue = {};
    let serverCache = this._getCacheValue(CacheService.KEY, defaultValue);
    this.cache.rehydrate(serverCache);
  }

  _getCacheValue(key: string, defaultValue: any): any {
    // browser
    const win: any = window;
    if (win[UNIVERSAL_KEY] && win[UNIVERSAL_KEY][key]) {
      let serverCache = defaultValue;
      try {
        serverCache = JSON.parse(win[UNIVERSAL_KEY][key]);
        if (typeof serverCache !== typeof defaultValue) {
          console.log('Angular Universal: The type of data from the server is different from the default value type');
          serverCache = defaultValue;
        }
      } catch (e) {
        console.log('Angular Universal: There was a problem parsing the server data during rehydrate');
        serverCache = defaultValue;
      }
      return serverCache;
    } else {
      console.log('Angular Universal: UNIVERSAL_CACHE is missing');
      console.log(win[UNIVERSAL_KEY]);
    }
    return defaultValue;
  }
}
