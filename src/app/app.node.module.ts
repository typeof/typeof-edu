import { NgModule } from '@angular/core';

import { commonImports, commonDeclarations } from './app.module';

import { RouterStoreModule } from '@ngrx/router-store';
import { StoreModule, Store } from '@ngrx/store';
import { rootReducer, AppState } from './reducers';

import { Http, RequestOptions } from '@angular/http';
import { AuthHttp, AuthConfig } from 'angular2-jwt';

import { Title } from '@angular/platform-browser';

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig({
    tokenGetter:(() => {
      const req = Zone.current.get('req') || {};
      if (req && req.cookies && req.cookies.id_token) {
        return req.cookies.id_token;
        // return tokenNotExpired(null, this.req.cookies.id_token)
      }
      return undefined;
    }),
      // return localStorage.getItem('id_token')})
  }), http, options);
}

import { AppComponent } from './index';
import { CacheService } from './shared/index';
import { ClientAuthService } from './shared/auth/client-auth.service';
import { ServerAuthService } from './shared/auth/server-auth.service';

export function getLRU() {
  return new Map();
}
export function getRequest() {
  const req = Zone.current.get('req') || {};
  return req;
}
export function getResponse() {
  return Zone.current.get('res') || {};
}

export const UNIVERSAL_KEY = 'UNIVERSAL_CACHE';

/**
 * Top-level NgModule "container"
 */
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    commonDeclarations,
    AppComponent,
  ],
  imports: [
    commonImports,
    RouterStoreModule.connectRouter(),
    StoreModule.provideStore(rootReducer),
  ],
  providers: [
    Title,
    { provide: 'req', useFactory: getRequest },
    { provide: 'res', useFactory: getResponse },
    { provide: 'LRU', useFactory: getLRU, deps: [] },

    CacheService,
    {
      provide: ClientAuthService,
      // useFactory: authHttpServiceFactory,
      useClass: ServerAuthService
      // useFactory: () => new ServerAuthService()
      // deps: [Http, RequestOptions]
    },
    {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions]
    },
  ],
})

export class AppModule {
  constructor(
    public cache: CacheService,
    private store: Store<AppState>
  ) {}

  /**
  * We need to use the arrow function here to bind the context as this is a gotcha
  * in Universal for now until it's fixed
  */
  universalDoDehydrate = (universalCache) => {

    universalCache[CacheService.KEY] = JSON.stringify(this.cache.dehydrate());

    // this.store.subscribe((state: any) => {
    this.store.select('secondNav').subscribe((state: any) => {
      universalCache['store'] = JSON.stringify(state);
    });
  }

  /**
  * Clear the cache after it's rendered
  */
  universalAfterDehydrate = () => {
    // comment out if LRU provided at platform level to be shared between each user
    this.cache.clear();
  }
}
