import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserMaterialsComponent } from './user-materials/user-materials.component';
import { UserMaterialsDetailComponent } from './user-materials-detail/user-materials-detail.component';
import { UserSectionsComponent } from './user-sections/user-sections.component';
import { SectionComponent } from './section/section.component';

import { NoAuthGuard } from '../no-auth.guard';

const routes: Routes = [
  { path: 'home/materials', component: UserMaterialsComponent, canActivate: [NoAuthGuard] },
  { path: 'home/materials/:status', component: UserMaterialsDetailComponent, canActivate: [NoAuthGuard] },
  { path: 'home/sections', component: UserSectionsComponent, canActivate: [NoAuthGuard] },
  { path: 'home/sections/:alias', component: SectionComponent, canActivate: [NoAuthGuard] },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes,{
    initialNavigation: false,
  }) ],
  providers: [ NoAuthGuard ],
  exports: [ RouterModule ]
})
export class HomeRoutingModule { }
