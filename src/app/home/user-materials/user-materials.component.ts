import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { AppState } from '../../reducers';

import {
  Status,
  Category,
  Material,
  MaterialsService,
  MaterialStatusType,
} from '../../shared/';
import { SecondNavService } from '../../shared/second-nav/second-nav.service';
import { ClientAuthService } from '../../shared/auth/client-auth.service';

@Component({
  selector: 'app-user-materials',
  templateUrl: './user-materials.component.html',
  styleUrls: ['./user-materials.component.scss']
})
export class UserMaterialsComponent implements OnInit {

  private materialsStatus: Status<Material[]>;
  private tabs = [{
    url: '/home/materials',
    title: 'Мои материалы',
    active: true,
    icon: 'coffee',
  }, {
    url: '/home/sections',
    title: 'Мои разделы',
    active: false,
    icon: 'list',
  }];

  private backlogMaterials: Material[] = [];
  private inProgressMaterials: Material[] = [];
  private doneMaterials: Material[] = [];
  
  private backlogStats: Category = {};
  private inProgressStats: Category = {};
  private doneStats: Category = {};

  private materialType = MaterialStatusType;

  constructor(
    private materialsService: MaterialsService,
    private authService: ClientAuthService,
    private router: Router,
    private secondNavService: SecondNavService,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    this.store.dispatch(this.secondNavService.changeNav({
      tabs: this.tabs,
    }));

    this.materialsService.getMaterialsByUser()
      .then((status: Status<Material[]>) => {
        this.materialsStatus = status;
        this.filterMaterials();
      });
  }

  filterMaterials() {
    this.materialsStatus.data = this.materialsStatus.data.map(material => new Material(material));
    this.backlogMaterials = this.materialsStatus.data.filter(material => 
      material.status === this.materialType.backlog
    );
    this.inProgressMaterials = this.materialsStatus.data.filter(material => 
      material.status === this.materialType.inProgress
    );
    this.doneMaterials = this.materialsStatus.data.filter(material => 
      material.status === this.materialType.done
    );

    this.backlogStats = {
      books: this.backlogMaterials.filter(mat => mat.book).length,
      videos: this.backlogMaterials.filter(mat => mat.video).length,
      courses: this.backlogMaterials.filter(mat => mat.course).length,
      articles: this.backlogMaterials.filter(mat => mat.article).length,
    };
    this.inProgressStats = {
      books: this.inProgressMaterials.filter(mat => mat.book).length,
      videos: this.inProgressMaterials.filter(mat => mat.video).length,
      courses: this.inProgressMaterials.filter(mat => mat.course).length,
      articles: this.inProgressMaterials.filter(mat => mat.article).length,
    };
    this.doneStats = {
      books: this.doneMaterials.filter(mat => mat.book).length,
      videos: this.doneMaterials.filter(mat => mat.video).length,
      courses: this.doneMaterials.filter(mat => mat.course).length,
      articles: this.doneMaterials.filter(mat => mat.article).length,
    };
  }

}
