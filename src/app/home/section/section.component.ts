import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Store } from '@ngrx/store';

import {
  Status,
  SectionsService,
  Material,
  MaterialsService,
  Category,
} from '../../shared/';
import { AppState } from '../../reducers';
import { ClientAuthService } from '../../shared/auth/client-auth.service';
import { SecondNavService } from '../../shared/second-nav/second-nav.service';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.scss']
})
export class SectionComponent implements OnInit {

  private materialsPromise: Promise<Status<Material[]>>;
  private penantTitle: string = 'Персональный раздел';
  private color: string = '#aedd94';
  private category: Category = {
    title: 'На каниулы',
  };

  private back = {
    btn: true,
    url: '/home/sections',
    title: 'Разделы',
  };

  constructor(
    private authService: ClientAuthService,
    private router: Router,
    private route: ActivatedRoute,
    private secondNavService: SecondNavService,
    private materialsService: MaterialsService,
    private sectionsService: SectionsService,
    private store: Store<AppState>,
  ) { }

  ngOnInit() {
    this.store.dispatch(this.secondNavService.changeNav({
      back: this.back,
    }));


    const alias = this.route.snapshot.params['alias'];

    this.sectionsService.getByAlias(alias)
      .then(status => {
        if (status.success) {
          this.category.title = status.data.title;
        }

        this.store.dispatch(this.secondNavService.changeNav({
          back: this.back,
          title: status.data.title,
        }));
      });

    this.materialsPromise = this.materialsService.getMaterialsByUserSection(alias);
  }

}
