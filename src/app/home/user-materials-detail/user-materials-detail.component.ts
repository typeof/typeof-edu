import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Store } from '@ngrx/store';

import {
  Status,
  Material,
  Category,
  CategoriesService,
  MaterialsService,
  MaterialStatusType,
} from '../../shared/';
import { AppState } from '../../reducers';
import { ClientAuthService } from '../../shared/auth/client-auth.service';
import { SecondNavService } from '../../shared/second-nav/second-nav.service';

@Component({
  selector: 'app-user-materials-detail',
  templateUrl: './user-materials-detail.component.html',
  styleUrls: ['./user-materials-detail.component.scss']
})
export class UserMaterialsDetailComponent implements OnInit {

  private materialsPromise: Promise<Status<Material[]>>;
  private penantTitle: string = 'Стадия изучения';
  private color: string = 'rgba(229,115,115,0.84)';
  private status: MaterialStatusType;
  private category: Category = {
    title: 'Планирую изучить',
  };

  private penantTitles = {
    [MaterialStatusType.backlog]: 'Стадия планирования',
    [MaterialStatusType.inProgress]: 'Стадия изучения',
    [MaterialStatusType.done]: 'Стадия завершённости',
  }

  private titles = {
    [MaterialStatusType.backlog]: 'Планирую изучить',
    [MaterialStatusType.inProgress]: 'Делаю',
    [MaterialStatusType.done]: 'Сделал',
  }
  // private materialsPromise: Status<Material[]>;

  private tabs = [{
    url: '/home/materials/backlog',
    title: 'Планирую',
    active: false,
  }, {
    url: '/home/materials/inProgress',
    title: 'Делаю',
    active: false,
  }, {
    url: '/home/materials/done',
    title: 'Завершенно',
    active: false,
  }];
  
  constructor(
    private materialsService: MaterialsService,
    private authService: ClientAuthService,
    private router: Router,
    private route: ActivatedRoute,
    private secondNavService: SecondNavService,
    private store: Store<AppState>,
  ) { }

  ngOnInit() {
    // let status: number = +MaterialType[this.route.snapshot.params['status']];

    this.route.params.forEach(params => {
      this.status = +MaterialStatusType[params['status']];
      this.init();
    });
  }

  init() {
    this.tabs[this.status - 1].active = true;

    this.store.dispatch(this.secondNavService.changeNav({
      tabs: this.tabs,
    }));

    this.penantTitle = this.penantTitles[this.status];
    this.category.title = this.titles[this.status];

    this.materialsPromise = this.materialsService.getMaterialsByUserStatus(this.status);
  }

}
