import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';

import { SharedModule } from '../shared/shared.module';

import { UserMaterialsComponent } from './user-materials/user-materials.component';
import { HomeRoutingModule } from './home-routing.module';
import { UserMaterialsDetailComponent } from './user-materials-detail/user-materials-detail.component';
import { UserSectionsComponent } from './user-sections/user-sections.component';
import { SectionComponent } from './section/section.component';
import { SectionsHeaderComponent } from './user-sections/sections-header/sections-header.component';
import { SectionBlockComponent } from './user-sections/section-block/section-block.component';
import { AddSectionComponent } from './user-sections/add-section/add-section.component';

@NgModule({
  imports: [
    MaterialModule,
    FormsModule,
    CommonModule,
    HomeRoutingModule,
    SharedModule,
  ],
  declarations: [
    UserMaterialsComponent,
    UserMaterialsDetailComponent,
    UserSectionsComponent,
    SectionComponent,
    SectionsHeaderComponent,
    SectionBlockComponent,
    AddSectionComponent,
  ],
})
export class HomeModule { }
