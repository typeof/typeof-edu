import { Component, OnInit, Input } from '@angular/core';

import { Section } from '../../../shared';

@Component({
  selector: 'app-section-block',
  templateUrl: './section-block.component.html',
  styleUrls: ['./section-block.component.scss']
})
export class SectionBlockComponent implements OnInit {

  @Input() section: Section;
  @Input() auto: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  getProgressValue(): number {
    return +this.section.backlog + +this.section.inProgress;
  }

  getProgressMax(): number {
    return +this.section.backlog + +this.section.inProgress + +this.section.done;
  }

  getPercentage() {
    return ((this.getProgressValue() / this.getProgressMax()) * 100 || 0).toFixed();
  }

}
