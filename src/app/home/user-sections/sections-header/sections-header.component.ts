import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';

@Component({
  selector: 'app-sections-header',
  templateUrl: './sections-header.component.html',
  styleUrls: ['./sections-header.component.scss']
})
export class SectionsHeaderComponent implements OnInit {

  @Input() title: string;
  @Input() count: number = 0;
  @Input() auto: boolean = false;
  @Input() showAddSection: boolean;
  @Output() showAddSectionChange = new EventEmitter<boolean>();

  private sectionWord: string;

  constructor() { }

  ngOnInit() {
    const countBy100 = this.count % 100;
    const countBy10 = countBy100 % 10;
    if (countBy100 >= 10 && countBy100 <= 20) {
      this.sectionWord = 'разделов';
    } else if (countBy10 === 1) {
      this.sectionWord = 'раздел';
    } else if (countBy10 >= 2 && countBy10 <= 4) {
      this.sectionWord = 'раздела';
    } else {
      this.sectionWord = 'разделов';
    }
  }

  show() {
    this.showAddSection = true;
    this.showAddSectionChange.emit(this.showAddSection)
  }

}
