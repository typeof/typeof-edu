import {
  Component,
  Output,
  EventEmitter,
  OnInit,
  OnDestroy,
} from '@angular/core';
import { isBrowser, isNode } from 'angular2-universal';

import {
  Section,
  SectionsService,
} from '../../../shared';

@Component({
  selector: 'app-add-section',
  templateUrl: './add-section.component.html',
  styleUrls: ['./add-section.component.scss']
})
export class AddSectionComponent implements OnInit, OnDestroy {

  @Output() onClose = new EventEmitter<boolean>();

  private newSection: Section = new Section();
  private error: string;

  private colorsList = [
    '#c0c0c0',
    '#aedd94',
    '#0079bf',
    '#eb5a46',
    '#ffab4a',
    '#00c8f8',
    '#f2d600',
  ];

  constructor(
    private sectionsService: SectionsService
  ) { }

  ngOnInit() {
    this.newSection.color = this.colorsList[0];
    if (isBrowser) {
      document.body.style.overflow = 'hidden';
    }
  }

  ngOnDestroy() {
    if (isBrowser) {
      document.body.style.overflow = 'auto';
    }
  }

  setColor(color: string) {
    this.newSection.color = color;
  }

  close(created: boolean) {
    this.onClose.emit(created);
  }

  stopProp(event) {
    event.stopPropagation();
  }

  addSection() {
    this.sectionsService
      .addSection(this.newSection)
      .then(status => {
        if (status.success) {
          this.close(true);
        } else {
          this.error = status.msg;
        }
      })
      .catch(() => this.close(false));
  }

}
