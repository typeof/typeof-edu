import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';

import {
  Status,
  Section,
  SectionsService,
  // Material,
  // MaterialsService,
  // MaterialType,
} from '../../shared/';
import { SecondNavService } from '../../shared/second-nav/second-nav.service';
import { ClientAuthService } from '../../shared/auth/client-auth.service';

@Component({
  selector: 'app-user-sections',
  templateUrl: './user-sections.component.html',
  styleUrls: ['./user-sections.component.scss']
})
export class UserSectionsComponent implements OnInit {

  private tabs = [{
    url: '/home/materials',
    title: 'Мои материалы',
    active: false,
    icon: 'coffee',
  }, {
    url: '/home/sections',
    title: 'Мои разделы',
    active: true,
    icon: 'list',
  }];

  private userSectionsStatus: Status<Section[]>;
  private generatedSectionsStatus: Status<Section[]>;
  private showAddSection: boolean = false;

  constructor(
    private sectionsService: SectionsService,
    private authService: ClientAuthService,
    private router: Router,
    private secondNavService: SecondNavService,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    this.store.dispatch(this.secondNavService.changeNav({
      tabs: this.tabs,
    }));

    this.loadUserSetions();

    this.sectionsService.getGeneratedSections()
      .then((status: Status<Section[]>) => {
        this.generatedSectionsStatus = status;
      });
  }

  loadUserSetions() {
    this.sectionsService.getUserSections()
      .then((status: Status<Section[]>) => {
        this.userSectionsStatus = status;
      });
  }

  onClose(created: boolean) {
    this.showAddSection = false;
    if (created) {
      this.loadUserSetions();
    }
  }

}
