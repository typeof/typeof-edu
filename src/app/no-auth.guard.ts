import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { ClientAuthService } from './shared/auth/client-auth.service';

@Injectable()
export class NoAuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: ClientAuthService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {

    if (!this.authService.authenticated()) {
      this.router.navigate(["/"]);
      return false;
    } else {
      return this.authService.isAdmin();
    }
  }
}
