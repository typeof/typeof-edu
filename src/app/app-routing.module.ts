import { NoAuthGuard } from './no-auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import { CategoriesListComponent } from './university/categories-list/categories-list.component';
import { AdminComponent } from './admin/admin/admin.component';

const routes: Routes = [
  { path: '', redirectTo: '/library/categories', pathMatch: 'full' },
  { path: 'library', redirectTo: '/library/categories', pathMatch: 'full' },
  { path: 'home', redirectTo: '/home/materials', pathMatch: 'full' },
  { path: 'admin', component: AdminComponent, canActivate: [NoAuthGuard] },
  // { path: 'categories', component: CategoriesListComponent },
  // {
  //   path: '',
  //   children: []
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  // imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [NoAuthGuard]
})
export class AppRoutingModule { }
