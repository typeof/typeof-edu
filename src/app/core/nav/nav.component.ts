import { SecondNavState } from './../../shared/second-nav/second-nav.reducer';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, RoutesRecognized } from '@angular/router';
import { isBrowser } from 'angular2-universal';
import { Store } from '@ngrx/store'
import { Location } from '@angular/common';;

import { ClientAuthService } from '../../shared/auth/client-auth.service';
import { AppState } from '../../reducers';
import { NavState } from './nav.reducer';
import { NavActions } from './nav.actions';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  navState: NavState;
  state: SecondNavState;

  private showAdmin: boolean = false;

  private currPath: string = '';
  private icon: string = '';

  private typeToStr = {
    book: 'Книга',
    video: 'Видео',
    course: 'Курс',
    article: 'Статья',
  };

  private typeToIcon = {
    book: 'book',
    video: 'youtube-play',
    course: 'graduation-cap',
    article: 'pencil',
  };

  constructor(
    private router: Router,
    private store: Store<AppState>,
    private navActions: NavActions,
    private location: Location,
    private auth: ClientAuthService
  ) {
  }

  ngOnInit() {
    this.store.select('secondNav').subscribe((state: any) => {
      this.state = state;
      if (this.state.title) {
        this.icon = this.typeToIcon[this.state.title];
        this.state.title = this.typeToStr[this.state.title];
      }
    });

    if (isBrowser && this.auth.authenticated()) {
      this.auth.isAdmin().then(isAdmin => {
        if (isAdmin) {
          this.showAdmin = true;
        } else {
          this.showAdmin = false;
        }
      });
    }
  }

  shouldShowBack() {
    return isBrowser && history.length > 1 && location.pathname !== '/categories';
  }

  navigateBack() {
    this.location.back();
  }
}
