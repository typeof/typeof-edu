import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Action } from '@ngrx/store';

import { NavState } from './nav.reducer';

@Injectable()
export class NavActions {

  static CHANGE_NAV = '[Nav] change';
  changeNav(navState: NavState): Action {
    return {
      type: NavActions.CHANGE_NAV,
      payload: navState,
    };
  }
  //
  // static EDIT_USER = '[User] Edit User';
  // editUser(user: User): Action {
  //   return {
  //     type: NavActions.EDIT_USER,
  //     payload: user
  //   };
  // }
  //
  // static LOGOUT = '[User] Logout';
  // logout(): Action {
  //   return {
  //     type: NavActions.LOGOUT
  //   };
  // }
  //
  // static LOGOUT_FAIL = '[User] Logout Fail';
  // logoutFail(err: Error): Action {
  //   return {
  //     type: NavActions.LOGOUT_FAIL,
  //     payload: err
  //   };
  // }
  //
  // static LOGOUT_SUCCESS = '[User] Logout Success';
  // logoutSuccess(res: Response): Action {
  //   return {
  //     type: NavActions.LOGOUT_SUCCESS,
  //     payload: res
  //   };
  // }
}
