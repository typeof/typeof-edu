
import { Action } from '@ngrx/store';

import { NavActions } from './nav.actions';
// import { User } from './user.model';

export interface NavState {
  currentPath: string;
  currentTitle: string;
  prevPath: string;
  prevTitle: string;
};

export const initialState: NavState = {
  currentPath: '',
  currentTitle: '',
  prevPath: '',
  prevTitle: '',
};

export function navReducer(state = initialState, action: Action): NavState {
  switch (action.type) {

    case NavActions.CHANGE_NAV: {
      return {
        prevTitle: action.payload.prevTitle || state.currentTitle,
        prevPath: action.payload.prevPath || state.currentPath,
        currentTitle: action.payload.currentTitle,
        currentPath: action.payload.currentPath,
      }
    }

    default: {
      return state;
    }
  }
}
