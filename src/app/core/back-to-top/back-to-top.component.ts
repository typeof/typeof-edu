import { Component } from '@angular/core';
import { isBrowser, isNode } from 'angular2-universal';

@Component({
  selector: 'app-back-to-top',
  templateUrl: './back-to-top.component.html',
  styleUrls: ['./back-to-top.component.scss']
})
export class BackToTopComponent {

  private active = false;

  constructor() { }

  scrollToTop(scrollDuration) {
    window.scrollTo(0, 0);
  }

}
