import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { NavComponent } from './nav/nav.component';
// import { SecondNavComponent } from './nav/second-nav.component';
import { NavActions } from './nav/nav.actions';
import { BackToTopComponent } from './back-to-top/back-to-top.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    InfiniteScrollModule,
  ],
  exports: [NavComponent, BackToTopComponent],
  declarations: [NavComponent, BackToTopComponent],
  providers: [NavActions],
})
export class CoreModule { }
