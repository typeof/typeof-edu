import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-category-live-tags',
  templateUrl: './category-live-tags.component.html',
  styleUrls: ['./category-live-tags.component.scss']
})
export class CategoryLiveTagsComponent implements OnInit {

  @Input() tags: {tag: string, count: number}[] = [];
  @Input() title: string = '';
  @Input() categoryAlias: string = '';
  @Input() short: boolean = false;
  @Input() addedMaterials: boolean = false;
  @Input() byPlatform: boolean = false;
  @Input() activeTags: string[] = [];
  @Output() onTagToggle = new EventEmitter<string>();

  private showAll: boolean = false;

  constructor() { }

  ngOnInit() {

  }

}
