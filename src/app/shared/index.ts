import { OpaqueToken } from '@angular/core';

export { Status } from './status.model';
export { CacheService } from './cache.service';
export { ClientAuthService } from './auth/client-auth.service';
export { ServerAuthService } from './auth/server-auth.service';

export const AuthService = new OpaqueToken("AuthService");

export { CollectionsService } from './collections/collections.service';
export { CategoriesService } from './categories/categories.service';
export { SectionsService } from './sections/sections.service';
export { MaterialsService } from './materials/materials.service';
export { Section } from './sections/section.model';
export {
  Collection,
  CollectionBlock,
  CollectionBlockType,
} from './collections/collection.model';
export {
  Category,
  CategoryType,
  Categories,
  tagsWhiteList,
} from './categories/category.model';
export {
  Material,
  CourseAuthor,
  MaterialType,
  MaterialStatusType,
  MaterialChangeEvent,
  TinyMaterial,
} from './materials/material.model';

export { MaterialsListComponent } from './materials-list/materials-list.component';
export { MaterialsSidebarComponent } from './materials-list/materials-sidebar/materials-sidebar.component';
export { MaterialBlockComponent } from './materials-list/material-block/material-block.component';