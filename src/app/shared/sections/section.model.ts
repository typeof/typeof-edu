import { Material } from '../materials/material.model';

export class Section {
  id?: string = '';
  alias: string = '';
  title: string = '';
  color?: string = '';
  media?: string = 'https://angular.io/resources/images/logos/angular2/angular.svg';
  user_id?: string = '';

  books?: number = 0;
  videos?: number = 0;
  courses?: number = 0;
  articles?: number = 0;

  backlog?: number = 0;
  inProgress?: number = 0;
  done?: number = 0;
}
