import { Injectable } from '@angular/core';
import { isBrowser, isNode } from 'angular2-universal'
import { Headers, Http } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';

import {
  Status,
  Section,
  Material,
} from '../';
import { CacheService } from '../cache.service';

@Injectable()
export class SectionsService {

  private sectionsUrl = '/api/sections';

  constructor(
    private http: Http,
    private authHttp: AuthHttp,
    private _cache: CacheService
  ) { }

  addMaterial(material: Material, section: Section) {
    const url = `${this.sectionsUrl}/addMaterial`;

    return this.authHttp
      .post(url, {
        material_id: material.id,
        section_id: section.id,
      })
      .toPromise()
      .then(res => console.log(res))
      .catch(this.handleError);
  }

  addSection(section: Section): Promise<Status<null>> {
    return this.authHttp
      .post(this.sectionsUrl, section)
      .toPromise()
      .then(res => res.json() as Status<null>)
      .catch(this.handleError);
  }

  getUserSections(): Promise<Status<Section[]>> {
    const url = `${this.sectionsUrl}/getUserSections`;

    if (this._cache.has(url)) {
      return this._cache.getAndRemove(url);
    }

    return this.authHttp
      .get(url)
      .toPromise()
      .then(response => response.json() as Status<Section[]>)
      .then((status: Status<Section[]>) => {
        if (isNode) {
          this._cache.set(url, status);
        }
        return status;
      })
      .catch(this.handleError);
  }

  getGeneratedSections(): Promise<Status<Section[]>> {
    const url = `${this.sectionsUrl}/getGeneratedSections`;

    if (this._cache.has(url)) {
      return this._cache.getAndRemove(url);
    }

    return this.authHttp
      .get(url)
      .toPromise()
      .then(response => response.json() as Status<Section[]>)
      .then((status: Status<Section[]>) => {
        if (isNode) {
          this._cache.set(url, status);
        }
        return status;
      })
      .catch(this.handleError);
  }

  getByAlias(alias: string): Promise<Status<Section>> {
    const url = `${this.sectionsUrl}/getByAlias/${alias}`;

    if (this._cache.has(url)) {
      return this._cache.getAndRemove(url);
    }

    return this.authHttp
      .get(url)
      .toPromise()
      .then(response => response.json() as Status<Section>)
      .then((status: Status<Section>) => {
        if (isNode) {
          this._cache.set(url, status);
        }
        return status;
      })
      .catch(this.handleError);
  }


  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
