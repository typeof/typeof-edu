import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { isBrowser, isNode } from 'angular2-universal'
import { Location } from '@angular/common';

import 'rxjs/add/operator/pairwise';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { SecondNavState } from './second-nav.reducer';
import { SecondNavService } from './second-nav.service';

@Component({
  selector: 'app-second-nav',
  templateUrl: './second-nav.component.html',
  styleUrls: ['./second-nav.component.scss']
})
export class SecondNavComponent implements OnInit {

  private showMain: boolean = false;
  private showSemi: boolean = false;
  private titleSemi: string = '';

  // state: SecondNavState;

  constructor(
    private route: ActivatedRoute,
    private store: Store<AppState>,
    // private router: Router,
    // private location: Location
  ) {}

  ngOnInit() {
    this.store.select('secondNav').subscribe((state: any) => {
      if (isBrowser) {
        this.showMain = false;
        this.showSemi = false;
        if (location.pathname.indexOf('library/categories/business') !== -1) {
          this.showSemi = !localStorage['helpInfoSemi'];
          this.titleSemi = 'бизнесу';
        } else if (location.pathname.indexOf('library/categories/history') !== -1) {
          this.showSemi = !localStorage['helpInfoSemi'];
          this.titleSemi = 'истории';
        } else if (location.pathname.indexOf('library/categories/science') !== -1) {
          this.showSemi = !localStorage['helpInfoSemi'];
          this.titleSemi = 'науке';
        } else if (location.pathname.indexOf('library/categories/tech') !== -1) {
          this.showSemi = !localStorage['helpInfoSemi'];
          this.titleSemi = 'технологиям';
        } else if (location.pathname.indexOf('library/platforms/arzamas') !== -1) {
          this.showSemi = !localStorage['helpInfoSemi'];
          this.titleSemi = 'Арзамасу';
        } else if (location.pathname.indexOf('library/platforms/postnauka') !== -1) {
          this.showSemi = !localStorage['helpInfoSemi'];
          this.titleSemi = 'ПостНауке';
        } else if (location.pathname.indexOf('library/material') === -1) {
          this.showMain = !localStorage['helpInfoClosed'];
        } else {
          this.showMain = false;
        }
      }
    });
  }

  close() {
    if (isBrowser) {
      if (this.showSemi) {
        localStorage.setItem('helpInfoSemi', 'true');
      } else {
        localStorage.setItem('helpInfoClosed', 'true');
      }
    }

    this.showSemi = false;
    this.showMain = false;
  }
}
