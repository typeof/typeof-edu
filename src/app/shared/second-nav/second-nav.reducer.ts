import { isBrowser, isNode } from 'angular2-universal'
import { Action } from '@ngrx/store';

import { SecondNavService } from './second-nav.service';
// import { User } from './user.model';

export interface SecondNavState {
  back?: {
    btn: boolean,
    title: string,
    url: string,
    icon?: string,
  };
  title?: string;
  titleIcon?: string;
  tabs?: {
    url: string,
    title: string,
    active: boolean,
    icon?: string,
  }[];
};

let init: any;

if (isBrowser && window['UNIVERSAL_CACHE'] && window['UNIVERSAL_CACHE']['store']) {
  try {
    init = JSON.parse(window['UNIVERSAL_CACHE']['store']);
    // console.log(init);
  } catch (e) {
    console.log(e);
  }
}

export const initialState: SecondNavState = init ? init : {
  back: {
    btn: false,
    title: '',
    url: '',
  },
  title: '',
  tabs: [],
};

export function secondNavReducer(state = initialState, action: Action): SecondNavState {
  switch (action.type) {

    case SecondNavService.CHANGE_SECOND_NAV: {
      return action.payload;
      // return {
      //   ...state,
      //   ...action.payload
      // }
    }

    default: {
      return state;
    }
  }
}
