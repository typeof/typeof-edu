import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Action } from '@ngrx/store';

import { SecondNavState } from './second-nav.reducer';

@Injectable()
export class SecondNavService {

  static CHANGE_SECOND_NAV = '[Second-Nav] change';
  changeNav(secondNavState: SecondNavState): Action {
    return {
      type: SecondNavService.CHANGE_SECOND_NAV,
      payload: secondNavState,
    };
  }
  //
  // static EDIT_USER = '[User] Edit User';
  // editUser(user: User): Action {
  //   return {
  //     type: SecondNavService.EDIT_USER,
  //     payload: user
  //   };
  // }
  //
  // static LOGOUT = '[User] Logout';
  // logout(): Action {
  //   return {
  //     type: SecondNavService.LOGOUT
  //   };
  // }
  //
  // static LOGOUT_FAIL = '[User] Logout Fail';
  // logoutFail(err: Error): Action {
  //   return {
  //     type: SecondNavService.LOGOUT_FAIL,
  //     payload: err
  //   };
  // }
  //
  // static LOGOUT_SUCCESS = '[User] Logout Success';
  // logoutSuccess(res: Response): Action {
  //   return {
  //     type: SecondNavService.LOGOUT_SUCCESS,
  //     payload: res
  //   };
  // }
}
