import { Injectable } from '@angular/core';
import { isBrowser, isNode } from 'angular2-universal'
import { Headers, Http } from '@angular/http';
// import { AuthHttp }      from 'angular2-jwt';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';

import {
  Status,
  Category,
  Categories,
  CategoryType,
} from '../';
import { CacheService } from '../cache.service';

@Injectable()
export class CategoriesService {

  private categoriesUrl = '/api/categories';

  constructor(
    private http: Http,
    private _cache: CacheService
  ) { }

  getCategory(alias: string, byPlatform: boolean = false): Promise<Status<Category>> {

    const url = `${this.categoriesUrl}/getByAlias/${byPlatform ? 'byPlatform/' : '' }${alias}`;

    if (this._cache.has(url)) {
      return this._cache.getAndRemove(url);
    }

    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Status<Category>)
      .then((status: Status<Category>) => {
        if (isNode) {
          this._cache.set(url, status);
        }
        return status;
      })
      .catch(this.handleError);
  }

  getAllCategories(byPlatform: boolean = false): Promise<Status<Category[]>> {

    const url = `${this.categoriesUrl}${byPlatform ? '/byPlatform' : ''}`;

    if (this._cache.has(url)) {
      return this._cache.getAndRemove(url);
    }


    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Status<Category[]>)
      .then((status: Status<Category[]>) => {
        if (!status.success) {
          return status;
        }

        if (!status.data || !status.data.length) {
          return {
            success: false,
            msg: 'Категорий не найдено',
            data: [],
          } as Status<Category[]>;
          // return new Status(false, 'Категорий не найдено');
        }

        if (isNode) {
          this._cache.set(url, status);
        }

        return status;

        // return new Status(status.success, status.msg, )
      })
      .catch(this.handleError);
  }

  getCategoriesByThemes(categories: Category[]): Category[] {
    return categories.filter(category =>
      category.type === CategoryType.theme
    );
  }

  getCategoriesByPlatform(categories: Category[]): Category[] {
    return categories.filter(category =>
      category.type === CategoryType.platform
    );
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
