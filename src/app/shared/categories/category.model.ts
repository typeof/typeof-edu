export enum CategoryType {
  none = 0,
  theme = 1,
  platform = 2,
};

export const tagsWhiteList = [
  // История
  'История России',
  'История древних времён',
  'Средневековье',
  'История западных стран',
  'История восточных стран',
  'Про войну',
  'Мировая история',
  'Философия',
  'История Америки',
  'Мифология',
  'Другое',

  // Бизнес
  'Стартап',
  'Продвижение',
  'маркетинг',
  'инвестиции',
  'продажи',
  'кейсы',
  'мотивация',
  'юриспруденция',
  'Личная эффективность',
  'Менеджмент',
  'Аналитика',
  'Контент',

  // Наука
  'Космос',
  'физика',
  'биология',
  'химия',
  'математика',

  // Тех
  'web-технологии',
  'PHP',
  'JavaScript',
  'Java',
  'Машинное обучение',
  'Базы данных',
  'C#',
  'C/C++',
  'Python',
  'Криптовалюты',
  'Нейронные сети',
  'Блокчейн',
  'Криптография',
  'Технологии',
  'ИИ',
];

export class Category {
  id?: string = '';
  alias?: string = '';
  title?: string = '';
  description?: string = '';
  tags?: string[] = [];
  type?: CategoryType = CategoryType.theme;
  media?: string;
  books?: number = 0;
  videos?: number = 0;
  courses?: number = 0;
  articles?: number = 0;
  communities_name?: string[] = [];
  communities_description?: string[] = [];
  communities_media?: string[] = [];
  communities_source?: string[] = [];
}

export interface Categories {
  categories: Category[];
  categoriesByThemes: Category[];
  categoriesByPlatform: Category[];
}
