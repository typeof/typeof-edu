export class MaterialFilters {
  book: boolean = false;
  video: boolean = false;
  course: boolean = false;
  article: boolean = false;

  backlog: boolean = true;
  inProgress: boolean = true;
  done: boolean = true;

  constructor() {
    this.book = false;
    this.video = false;
    this.course = false;
    this.article = false;

    this.backlog = true;
    this.inProgress = true;
    this.done = true;
  }
};

export class FilterBlocks {
  tags: boolean = false;
  planning: boolean = false;
  assignation?: boolean = false;
};

export enum AssignationType {
  All,
  Assigned,
  NotAssigned,
};
