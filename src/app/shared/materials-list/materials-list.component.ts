import { isBrowser } from 'angular2-universal';
import { Title } from '@angular/platform-browser';
import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import * as _ from 'underscore';

import {
  Status,
  Material,
  MaterialStatusType,
  MaterialChangeEvent,
  Category,
  Section,
  tagsWhiteList,
} from '../../shared/';
import {
  MaterialFilters,
  FilterBlocks,
  AssignationType,
} from './material-filters.model'

import { MaterialsService } from '../../shared/materials/materials.service';
import { ClientAuthService } from '../../shared/auth/client-auth.service';
import { SectionsService } from './../sections/sections.service';

@Component({
  selector: 'app-materials-list',
  templateUrl: './materials-list.component.html',
  styleUrls: ['./materials-list.component.scss']
})
export class MaterialsListComponent implements OnInit, OnChanges {

  @Input() color: string = 'rgba(62, 72, 109, 0.8)';
  @Input() status: MaterialStatusType;
  @Input() materialsPromise: Promise<Status<Material[]>>;
  @Input() category: Category;
  @Input() penantTitle: string = 'Каталог материалов';
  @Input() sortContentByRating: boolean = true;
  @Input() short: boolean = false;
  @Output() onContentSortChange = new EventEmitter<boolean>();

  private sections: Section[] = [];
  private materialFilters: MaterialFilters = new MaterialFilters();
  private filterBlocks: FilterBlocks = {
    tags: false,
    planning: false,
  };
  private error: string = '';
  private activeTags: string[] = [];
  private activePlatforms: string[] = [];
  private materialsStatus: Status<Material[]>;
  private filteredMaterials: Material[] = [];
  private cuttedMaterials: Material[] = [];
  private assignation: AssignationType = AssignationType.All;
  private pageLength: number = 20;
  private tagRemoved: boolean = false;
  private tags: {tag: string, count: number}[] = [];
  private platforms: {tag: string, count: number}[] = [];

  constructor(
    private route: ActivatedRoute,
    private materialsService: MaterialsService,
    private sectionsService: SectionsService,
    private authService: ClientAuthService,
    private titleService: Title
  ) { }

  ngOnInit() {

    if (this.authService.authenticated()) {
      this.filterBlocks.assignation = true;
      this.filterBlocks.planning = true;

      this.sectionsService.getUserSections().then((status) => {
        if (status.success) {
          this.sections = status.data;
        }
      })
    }

    if (this.status) {
      return this.route.params.forEach(params => {
        this.status = +MaterialStatusType[params['status']];
        this.initWithStatus();
      });
    } else {
      this.init();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['materialsPromise']) {
      this.init();
    }
  }

  init() {
    this.materialsPromise.then(status => {
      // this.filteredMaterials = status.data || [];
      this.materialsStatus = status;

      if (isBrowser) {
        this.titleService.setTitle(`${this.category.title} - Каталог | You Know`);
      }

      this.collectCategoryTags();
      this.setInitFilters();
      this.filterMaterials();
    })
  }

  setInitFilters() {
    if (this.tagRemoved) {
      return;
    }

    const tag = this.route.snapshot.params['tag'];

    if (
      tag === 'book' ||
      tag === 'video' ||
      tag === 'article' ||
      tag === 'course'
    ) {
      this.materialFilters[tag] = true;
      return;
    }

    if (tag && this.tags.filter(el => el.tag === tag).length && this.activeTags.indexOf(tag) === -1) {
      this.activeTags = [
        ...this.activeTags,
        tag,
      ];
    } else if (tag && this.platforms.filter(el => el.tag === tag).length && this.activePlatforms.indexOf(tag) === -1) {
      this.activePlatforms = [
        ...this.activePlatforms,
        tag,
      ];
    }
  }

  initWithStatus() {
    this.materialsService
      .getMaterialsByUserStatus(this.status)
      .then(status => {
        this.materialsStatus = status;
        this.filterMaterials();
        this.collectCategoryTags();
      });
  }

  onMaterialChanged({ status, action, material }: MaterialChangeEvent) {
    if (action === 'added') {
      this.materialsStatus.data = this.materialsStatus.data.map(curMaterial => {
        if (material.id === curMaterial.id) {
          curMaterial.added = true;
          curMaterial.status = MaterialStatusType.backlog;
        }
        return curMaterial;
      });

      if (this.status) {
        this.initWithStatus();
      }
    } else if (action === 'removed') {
      this.materialsStatus.data = this.materialsStatus.data.map(curMaterial => {
        if (material.id === curMaterial.id) {
          curMaterial.added = false;
        }
        return curMaterial;
      });
    } else if (action === 'status') {
      this.materialsStatus.data = this.materialsStatus.data.map(curMaterial => {
        if (material.id === curMaterial.id) {
          curMaterial.status = status;
        }
        return curMaterial;
      });

      if (this.status) {
        this.initWithStatus();
      }
    } else {
      this.error = 'something goes wrong';
    }

    this.filterMaterials();
  }

  onTagToggle(tag: string) {
    if (this.activeTags.indexOf(tag) !== -1) {
      if (this.route.snapshot.params['tag'] === tag) {
        if (this.route.snapshot.params['alias']) {
          history.pushState('','',`/library/categories/${this.route.snapshot.params['alias']}/catalog/`);
        } else {
          history.pushState('','','/library/my-materials/catalog/');
        }

        this.tagRemoved = true;
      }
      this.activeTags = this.activeTags.filter((el) => el !== tag);
    } else {
      this.activeTags = [
        ...this.activeTags,
        tag,
      ];
    }
    this.filterMaterials();
  }

  onPlatformToggle(tag: string) {
    if (this.activePlatforms.indexOf(tag) !== -1) {
      if (this.route.snapshot.params['tag'] === tag) {
        if (this.route.snapshot.params['alias']) {
          history.pushState('','',`/library/categories/${this.route.snapshot.params['alias']}/catalog/`);
        } else {
          history.pushState('','','/library/my-materials/catalog/');
        }

        this.tagRemoved = true;
      }
      this.activePlatforms = this.activePlatforms.filter((el) => el !== tag);
    } else {
      this.activePlatforms = [
        ...this.activePlatforms,
        tag,
      ];
    }
    this.filterMaterials();
  }

  filterMaterials() {

    this.pageLength = 20;

    this.filteredMaterials = this.materialsStatus.data
      .map(material => new Material(material))
      .filter(material => {
        if (
          !this.materialFilters.book &&
          !this.materialFilters.video &&
          !this.materialFilters.course &&
          !this.materialFilters.article
        ) {
          return true;
        } else {
          return this.materialFilters[material.typeName]
        }
      })
      .filter(material => {
        if (this.activeTags.length) {
          return material.tags.filter(tag => this.activeTags.indexOf(tag) !== -1).length;
        } else {
          return true;
        }
      })
      .filter(material =>
        this.activePlatforms.length ? this.activePlatforms.indexOf(material.platform) !== -1 : true
      )
      .filter(material => {
        if ((this.assignation === AssignationType.Assigned && !material.added) ||
          (this.assignation === AssignationType.NotAssigned && material.added)) {
          return false;
        }
        return true;
      });

    if (this.authService.authenticated()) {
      this.filteredMaterials = this.filteredMaterials
        .filter(material =>
          material.added ? this.materialFilters[material.statusName] : true
        );
    }

    this.category.books = this.filteredMaterials.filter(mat => mat.book).length;
    this.category.videos = this.filteredMaterials.filter(mat => mat.video).length;
    this.category.courses = this.filteredMaterials.filter(mat => mat.course).length;
    this.category.articles = this.filteredMaterials.filter(mat => mat.article).length;

    this.sliceMaterials();
  }

  flatMap<T, U>(array: T[], callbackfn: (value: T, index: number, array: T[]) => U[]): U[] {
    return [].concat(...array.map(callbackfn));
  }

  collectCategoryTags() {

    const flatTags = this.flatMap(
      this.materialsStatus.data,
      material => material.tags
    );

    const tagsObj = _.countBy(flatTags, (tag) => tag);

    this.tags = Object.keys(tagsObj)
      .map(tag => ({
        tag,
        count: tagsObj[tag],
      }))
      .filter(tag =>
        tagsWhiteList.indexOf(tag.tag) !== -1
      )
      .sort((a, b) =>
        b.count - a.count
      );

    const flatPlatforms = this.materialsStatus.data.map(material => material.platform);

    const platformsObj = _.countBy(flatPlatforms, (platform) => platform);

    this.platforms = Object.keys(platformsObj)
      .map(platform => ({
        tag: platform,
        count: platformsObj[platform],
      }))
      .filter(tag => tag.tag !== 'null')
      .sort((a, b) =>
        b.count - a.count
      );
  }

  sliceMaterials() {
    this.cuttedMaterials = this.filteredMaterials.slice(0, this.pageLength);
  }

  onMaterialFilterToggle(filter: string) {
    if (this.route.snapshot.params['tag'] === filter) {
      if (this.route.snapshot.params['alias']) {
        history.pushState('','',`/library/categories/${this.route.snapshot.params['alias']}/catalog/`);
      } else {
        history.pushState('','','/library/my-materials/catalog/');
      }
      this.tagRemoved = true;
    }

    this.materialFilters[filter] = !this.materialFilters[filter];
    this.filterMaterials();
  }

  onAssignationFilterToggle(filter: AssignationType) {
    this.assignation = filter;
    this.filterMaterials();
  }

  clearTagsFilters() {
    if (this.route.snapshot.params['tag'] && !this.tagRemoved) {
      if (this.route.snapshot.params['alias']) {
        history.pushState('','',`/library/categories/${this.route.snapshot.params['alias']}/catalog/`);
      } else {
        history.pushState('','','/library/my-materials/catalog/');
      }
      this.tagRemoved = true;
    }

    this.materialFilters = new MaterialFilters();
    this.activeTags = [];
    this.activePlatforms = [];
    this.filterMaterials();
  }

  getCategory(): Category {
    return this.category;
  }

  onScroll() {

    if (this.pageLength > this.materialsStatus.data.length) {
      return;
    }
    this.pageLength += 10;
    this.sliceMaterials();
  }
}
