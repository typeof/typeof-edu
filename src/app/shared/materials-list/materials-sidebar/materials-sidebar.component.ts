import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Category } from '../../../shared';
import { MaterialFilters, FilterBlocks, AssignationType } from '../material-filters.model'

@Component({
  selector: 'app-materials-sidebar',
  templateUrl: './materials-sidebar.component.html',
  styleUrls: ['./materials-sidebar.component.scss']
})
export class MaterialsSidebarComponent implements OnInit {

  @Input() category: Category;
  @Input() materialFilters: MaterialFilters;
  @Input() assignation: AssignationType = AssignationType.All;
  @Input() filterBlocks: FilterBlocks;
  @Input() tags: {tag: string, count: number}[] = [];
  @Input() platforms: {tag: string, count: number}[] = [];
  @Input() penantTitle: string = 'Каталог материалов';
  @Input() color: string = 'rgba(62, 72, 109, 0.8)';
  @Input() activeTags: string[] = [];
  @Input() activePlatforms: string[] = [];
  @Output() onTagToggle = new EventEmitter<string>();
  @Output() onPlatformToggle = new EventEmitter<string>();
  @Output() onMaterialFilterToggle = new EventEmitter<string>();
  @Output() onAssignationFilterToggle = new EventEmitter<AssignationType>();

  private assignationType = AssignationType;
  private filterBlocksShow: Object = {
    types: true,
  };

  constructor() {}

  ngOnInit() {}

  toggleBlock(blockName: string) {
    this.filterBlocksShow[blockName] = !this.filterBlocksShow[blockName];
  }

}
