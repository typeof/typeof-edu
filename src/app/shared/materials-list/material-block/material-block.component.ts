import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import {
  Material,
  Section,
  MaterialStatusType,
} from '../../../shared';

@Component({
  selector: 'app-material-block',
  templateUrl: './material-block.component.html',
  styleUrls: ['./material-block.component.scss']
})
export class MaterialBlockComponent implements OnInit {

  @Input() material: Material;
  @Input() extended: boolean = false;
  @Input() sections: Section[] = [];
  @Input() color: string = 'rgba(62, 72, 109, 0.8)';
  @Input() status: MaterialStatusType = MaterialStatusType.backlog;
  @Output() onMaterialChanged = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
    this.material = new Material(this.material);
  }

}
