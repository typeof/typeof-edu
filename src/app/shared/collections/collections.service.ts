import { ClientAuthService } from './../auth/client-auth.service';
import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { isBrowser, isNode } from 'angular2-universal'
import { Headers, Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';

import { Status, Collection } from '../';
import { CacheService } from '../cache.service';

@Injectable()
export class CollectionsService {

  private collectionsUrl = '/api/collections';

  constructor(
    private authHttp: AuthHttp,
    private http: Http,
    private authService: ClientAuthService,
    private _cache: CacheService
  ) { }

  getHttpAdapter() {
    return this.authService.authenticated() ? this.authHttp : this.http;
  }

  getAllCollections(): Promise<Status<Collection[]>> {

    const url = this.collectionsUrl;

    if (this._cache.has(url)) {
      return this._cache.getAndRemove(url);
    }

    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Status<Collection[]>)
      .then((status: Status<Collection[]>) => {
        if (isNode) {
          this._cache.set(url, status);
        }
        return status;
      })
      .catch(this.handleError);
  }

  getCollectionByAlias(alias: string): Promise<Status<Collection>> {
    const url = `${this.collectionsUrl}/getByAlias/${alias}`;

    if (this._cache.has(url)) {
      return this._cache.getAndRemove(url);
    }

    return this.getHttpAdapter().get(url)
      .toPromise()
      .then(response => response.json() as Status<Collection>)
      .then((status: Status<Collection>) => {
        if (isNode) {
          this._cache.set(url, status);
        }
        return status;
      })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
