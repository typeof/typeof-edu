import { Material } from '../materials/material.model';

export class Collection  {
  id: string = '';
  alias: string = '';
  title: string = '';
  tags: string = '';
  description: string = '';
  blocks?: CollectionBlock[] = [];
  // type: string = '';
  media?: string;
  books?: number = 0;
  videos?: number = 0;
  courses?: number = 0;
  articles?: number = 0;
}

export enum CollectionBlockType {
  none = 0,
  cover = 1,
  author = 2,
  cause = 3,
  target = 4,
  materials = 5,
  material = 6,
  materials_short = 7,
  quote = 8,
  sources = 9,
  recommendations = 10,
  video = 11,
  video_short = 12,
  articles = 13,
  course = 14,
  // video_short = 12,
};

export class CollectionBlock {
  id: string = '';
  collection_id: string = '';
  order: number = 0;
  type: CollectionBlockType = CollectionBlockType.none;
  title: string = '';
  background: string = '';
  texts_1: string[] = [];
  texts_2: string[] = [];
  medias: string[] = [];
  urls: string[] = [];
  materials_ids: string[] = [];
  materials?: Material[] = [];
}
