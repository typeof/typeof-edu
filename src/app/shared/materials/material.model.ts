import { pluralForm } from './../plural-form';
import { Category } from '../categories/category.model';
import { Collection } from '../collections/collection.model';

export class CourseAuthor {
  name: string;
  description: string;
  media: string;
}

export enum MaterialStatusType {
  backlog = 1,
  inProgress = 2,
  done = 3,
};

export class MaterialChangeEvent {
  material: Material;
  status?: MaterialStatusType;
  action: string;
};

const typeToString = [
  'none',
  'book',
  'video',
  'course',
  'article',
];

const typeToStatus = [
  'none',
  'backlog',
  'inProgress',
  'done',
];

export enum MaterialType {
  none = 0,
  book = 1,
  video = 2,
  course = 3,
  article = 4,
};

const typeToStringRu = {
  [MaterialType.book]: 'Книга',
  [MaterialType.video]: 'Видео',
  [MaterialType.course]: 'Курс',
  [MaterialType.article]: 'Статья',
};

const typeToPluralForm = {
  'book': ['книга', 'книги', 'книг'],
  'video': ['видео', 'видео', 'видео'],
  'course': ['курс', 'курс', 'курсов'],
  'article': ['статья', 'статьи', 'статей'],
}

export const materialPluralForm = (count: number, type: string): string => {
  return pluralForm(count, typeToPluralForm[type]);
}

const initMaterial = {
  id: '',
  title: '',
  type: MaterialType.none,
  media: '',
  media_video: '',
  duration: 0,
  description: '',
  alias: '',
  authors: [],
  authors_description: [],
  authors_media: [],
  tags: [],
  platform: '',
  lang: '',
  cost: 0,
  currency: '',
  full_description: '',
  structure: [],
  start_date: null,
  end_date: null,
  source: '',
  links: [],
  links_name: [],

  rating: 0,
  created_at: '',

  status: MaterialStatusType.backlog,
  courseAuthors: [],
  added: false,
  materials_ids: [],
  collections_ids: [],
  categories_ids: [],
  materials: [],
  collections: [],
  categories: [],

  date: new Date(),
  statusName: '',
  typeName: '',
  typeNameRu: '',
  book: false,
  video: false,
  course: false,
  article: false,
}

export class TinyMaterial {
  id: string = '';
  title: string = '';
  type: MaterialType = MaterialType.none;
  media?: string = '';
  description: string = '';
  alias: string = '';
  platform?: string = '';
  source?: string = '';

  added?: boolean = false;
  status: MaterialStatusType = MaterialStatusType.backlog;
}

export class Material {
  id: string = '';
  title: string = '';
  type: MaterialType = MaterialType.none;
  media?: string = '';
  media_video?: string = '';
  duration?: number = 0;
  description: string = '';
  alias: string = '';
  authors?: string[] = [];
  authors_description?: string[] = [];
  authors_media?: string[] = [];
  tags?: string[] = [];
  platform?: string = '';
  lang?: string = '';
  cost?: number = 0;
  currency?: string = '';
  full_description?: string = '';
  structure?: string[] = [];
  start_date?: Date;
  end_date?: Date;
  rating: number = 0;
  created_at: string = '';
  source?: string = '';
  links?:string[] = [];
  links_name?:string[] = [];

  status: MaterialStatusType = MaterialStatusType.backlog;
  courseAuthors?: CourseAuthor[] = [];
  added?: boolean = false;
  materials_ids: string[] = [];
  collections_ids: string[] = [];
  categories_ids: string[] = [];
  materials: TinyMaterial[] = [];
  collections: Collection[] = [];
  categories: Category[] = [];
  // duration: number;
  // categoies: [string];
  // collections: [string];

  // constructor()
  constructor(material: Material = initMaterial as Material) {
    this.id = material.id;
    this.title = material.title;
    this.type = material.type;
    this.media = material.media;
    this.media_video = material.media_video;
    this.duration = material.duration;
    this.description = material.description;
    this.alias = material.alias;
    this.authors = material.authors;
    this.authors_description = material.authors_description;
    this.authors_media = material.authors_media;
    this.tags = material.tags;
    this.platform = material.platform;
    this.lang = material.lang;
    this.cost = material.cost;
    this.currency = material.currency;
    this.full_description = material.full_description;
    this.structure = material.structure;
    this.start_date = material.start_date;
    this.end_date = material.end_date;
    this.rating = material.rating;
    this.created_at = material.created_at;
    this.status = material.status;
    this.courseAuthors = material.courseAuthors;
    this.added = material.added;
    this.materials_ids = material.materials_ids;
    this.collections_ids = material.collections_ids;
    this.categories_ids = material.categories_ids;
    this.materials = material.materials;
    this.collections = material.collections;
    this.categories = material.categories;
    this.source = material.source;
    this.links = material.links;
    this.links_name = material.links_name;
  }

  get date(): Date {
    return new Date(this.created_at);
  }

  get statusName(): string {
    return typeToStatus[this.status];
  }

  get typeName(): string {
    return typeToString[this.type];
  }

  get typeNameRu(): string {
    return typeToStringRu[this.type];
  }

  get book(): boolean {
    return this.type == MaterialType.book;
  }

  get video(): boolean {
    return this.type == MaterialType.video;
  }

  get course(): boolean {
    return this.type == MaterialType.course;
  }

  get article(): boolean {
    return this.type == MaterialType.article;
  }
}
