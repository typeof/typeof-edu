import { Injectable }    from '@angular/core';
import { isBrowser, isNode } from 'angular2-universal'
import { Headers, Http } from '@angular/http';
import { AuthHttp }      from 'angular2-jwt';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';

import {
  Status,
  Section,
  Category,
  Material,
  TinyMaterial,
  MaterialStatusType,
} from '../';
import { CacheService } from '../cache.service';

import { ClientAuthService } from '../../shared/auth/client-auth.service';

@Injectable()
export class MaterialsService {

  private materialsUrl = '/api/materials';

  constructor(
    private authHttp: AuthHttp,
    private http: Http,
    private _cache: CacheService,
    private authService: ClientAuthService
  ) { }

  getHttpAdapter() {
    return this.authService.authenticated() ? this.authHttp : this.http;
  }

  getAllMaterials(): Promise<Status<Material[]>> {
    const url = `${this.materialsUrl}`;

    if (this._cache.has(url)) {
      return this._cache.getAndRemove(url);
    }

    return this.getHttpAdapter()
      .get(url)
      .toPromise()
      .then(response => response.json() as Status<Material[]>)
      .then((status: Status<Material[]>) => {
        if (isNode) {
          this._cache.set(url, status);
        }
        return status;
      })
      .catch(this.handleError);
  }

  getMaterial(alias: string): Promise<Status<Material>> {
    const url = `${this.materialsUrl}/getByAlias/${alias}`;

    if (this._cache.has(url)) {
      return this._cache.getAndRemove(url);
    }

    return this.getHttpAdapter()
      .get(url)
      .toPromise()
      .then(response => response.json() as Status<Material>)
      .then((status: Status<Material>) => {
        if (isNode) {
          this._cache.set(url, status);
        }
        return status;
      })
      .catch(this.handleError);
  }

  getMaterialsByCategory(
    category: Category,
    offset: number,
    limit: number,
    sortByRating: boolean,
    byPlatform: boolean,
  ): Promise<Status<Material[]>> {
    const url = `${this.materialsUrl}/getByCategoryId/${byPlatform ? category.title : category.id}/${offset}/${limit}/${+sortByRating}/${+byPlatform}`;

    if (this._cache.has(url)) {
      return this._cache.getAndRemove(url);
    }

    return this.getHttpAdapter()
      .get(url)
      .toPromise()
      .then(response => response.json() as Status<Material[]>)
      .then((status: Status<Material[]>) => {
        if (isNode) {
          this._cache.set(url, status);
        }
        return status;
      })
      .catch(this.handleError);
  }

  getMaterialsByUser(): Promise<Status<Material[]>> {
    const url = `${this.materialsUrl}/getByUser`;

    if (this._cache.has(url)) {
      return this._cache.getAndRemove(url);
    }

    return this.authHttp
      .get(url)
      .toPromise()
      .then(response => response.json() as Status<Material[]>)
      .then((status: Status<Material[]>) => {
        if (isNode) {
          this._cache.set(url, status);
        }
        return status;
      })
      .catch(this.handleError);
  }

  getMaterialsByUserSection(alias: string): Promise<Status<Material[]>> {
    const url = `${this.materialsUrl}/getByUserSection/${alias}`;

    if (this._cache.has(url)) {
      return this._cache.getAndRemove(url);
    }

    return this.authHttp
      .get(url)
      .toPromise()
      .then(response => response.json() as Status<Material[]>)
      .then((status: Status<Material[]>) => {
        if (isNode) {
          this._cache.set(url, status);
        }
        return status;
      })
      .catch(this.handleError);
  }

  getMaterialsByUserStatus(status: number): Promise<Status<Material[]>> {
    const url = `${this.materialsUrl}/getByUserStatus/${status}`;

    if (this._cache.has(url)) {
      return this._cache.getAndRemove(url);
    }

    return this.authHttp
      .get(url)
      .toPromise()
      .then(response => response.json() as Status<Material[]>)
      .then((status: Status<Material[]>) => {
        if (isNode) {
          this._cache.set(url, status);
        }
        return status;
      })
      .catch(this.handleError);
  }

  changeStatus(material: Material, status: MaterialStatusType): Promise<Status<null>> {

    const url = `${this.materialsUrl}/changeStatus`;

    return this.authHttp.post(url, {
      material_id: material.id,
      status,
    })
      .toPromise()
      .then(response => response.json() as Status<null>)
      .then((status: Status<null>) => {
        this._cache.set(url, status);

        return status;
      })
      .catch(this.handleError);
  }

  addMaterialToUser(material: Material, status: MaterialStatusType): Promise<Status<null>> {

    const url = `${this.materialsUrl}/addToUser`;

    return this.authHttp.post(url, {
      material_id: material.id,
      status,
    })
      .toPromise()
      .then(response => response.json() as Status<null>)
      .then((status: Status<null>) => {
        this._cache.set(url, status);

        return status;
      })
      .catch(this.handleError);
  }

  removeMaterialFromUser(material: Material): Promise<Status<null>> {

    const url = `${this.materialsUrl}/removeFromUser`;

    return this.authHttp.post(url,{
      material_id: material.id,
    })
      .toPromise()
      .then(response => response.json() as Status<null>)
      .then((status: Status<null>) => {
        this._cache.set(url, status);

        return status;
      })
      .catch(this.handleError);
  }

  addAttachement(
    material: Material,
    attachedMaterial: TinyMaterial
  ): Promise<Status<null>> {
    const url = `${this.materialsUrl}/addAttachement`;

    return this.authHttp.post(url, {
      material_id_1: material.id,
      material_id_2: attachedMaterial.id,
    })
      .toPromise()
      .then(response => response.json() as Status<null>)
      .catch(err => console.log(err));
  }

  removeAttachement(
    material: Material,
    attachedMaterial: TinyMaterial
  ): Promise<Status<null>> {
    const url = `${this.materialsUrl}/removeAttachement`;

    return this.authHttp.post(url, {
      material_id_1: material.id,
      material_id_2: attachedMaterial.id,
    })
      .toPromise()
      .then(response => response.json() as Status<null>)
      .catch(err => console.log(err));
  }


  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
