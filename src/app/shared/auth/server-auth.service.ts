import { Injectable, Inject } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';

import * as auth0 from 'auth0-js';

import { CacheService } from '../cache.service';

@Injectable()
export class ServerAuthService {
  // lock = new Auth0Lock('wYoEnzt1QnChuh0iEGCn22JYuFHCqyfd', 'typeof.eu.auth0.com', {});

  // auth0Manage = new auth0.Management({
  //   domain:       'typeof.eu.auth0.com',
  //   clientID:     'wYoEnzt1QnChuh0iEGCn22JYuFHCqyfd'
  // });

  userProfile: Object;

  constructor(
    @Inject('req') private req,
    private http: Http,
    private _cache: CacheService
  ) {
    if (this.authenticated()) {
      this.http.get(`https://typeof.eu.auth0.com/tokeninfo?id_token=${this.req.cookies.id_token}`)
      .toPromise()
      .then(response => {
        this.userProfile = response.json()
        this._cache.set('profile', this.userProfile);
      })
    }
    // this.auth0Manage.getUser(userId, cb);
  }
  // constructor(private authenticationId, private isUserLoggedIn:boolean) { }

  isAdmin() {
    // throw new Error("isAdmin event cannot be called while doing server side rendering");
    return false;
  }

  login() {
    throw new Error("Login event cannot be called while doing server side rendering");
  }

  logout() {
    throw new Error("Logout event cannot be called while doing server side rendering");
  }

  authenticated() {
    if (this.req && this.req.cookies && this.req.cookies.id_token) {
      return tokenNotExpired(null, this.req.cookies.id_token)
    }

    return false;
    // throw new Error("Authenticated event cannot be called while doing server side rendering");
    // return this.isUserLoggedIn;
  }
}
