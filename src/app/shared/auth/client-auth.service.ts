import { Injectable } from '@angular/core';
import { tokenNotExpired, AuthHttp } from 'angular2-jwt';
import { isBrowser, isNode } from 'angular2-universal';
import { Cookie } from 'ng2-cookies';
import { Action } from '@ngrx/store';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';

import Auth0Lock from 'auth0-lock';

import { Store } from '@ngrx/store';

import { AppState } from '../../reducers';

import { CacheService } from '../cache.service';
import { AuthService } from './auth.service';

import { AuthState } from './auth.reducer';

@Injectable()
export class ClientAuthService implements AuthService {

  static AUTH_SIGN_IN = '[Auth] sign-in';
  static AUTH_SIGN_OUT = '[Auth] sign-out';

  signInEvent(): Action {
    return {
      type: ClientAuthService.AUTH_SIGN_IN,
      // payload: true,
    };
  }
  
  signOutEvent(): Action {
    return {
      type: ClientAuthService.AUTH_SIGN_OUT,
      // payload: false,
    };
  }

  // Configure Auth0
  lock = new Auth0Lock('wYoEnzt1QnChuh0iEGCn22JYuFHCqyfd', 'typeof.eu.auth0.com', {
    autoclose: true,
    auth: {
      redirect: false,
      responseType: 'token',
    },
    languageDictionary: {
      title: "You Know"
    },
    language: 'ru',
    theme: {
      logo: "https://u-know.ru/favicon-58x58.png",
      primaryColor: "#646c8a",
    }
  });

  //Store profile object in auth class
  userProfile: Object;

  constructor(
    private authHttp: AuthHttp,
    private _cache: CacheService,
    private store: Store<AppState>,
    private router: Router
  ) {
    // Set userProfile attribute of already saved profile
    if (this._cache.has('profile')) {
      Observable
        .of(this._cache.get('profile'))
        .toPromise()
        .then(res => {
          console.log(res);
          this.userProfile = res;
          return res;
        });
    } else {
      this.userProfile = JSON.parse(localStorage.getItem('profile'));
    }

    // Add callback for the Lock `authenticated` event
    this.lock.on("authenticated", (authResult) => {
      localStorage.setItem('id_token', authResult.idToken);
      Cookie.set('id_token', authResult.idToken, 30, '/');

      this.store.dispatch(this.signInEvent());

      // Fetch profile information
      this.lock.getProfile(authResult.idToken, (error, profile) => {
        if (error) {
          // Handle error
          alert(error);
          return;
        }

        localStorage.setItem('profile', JSON.stringify(profile));
        this.userProfile = profile;
      });
    });    
  }

  public login() {
    // Call the show method to display the widget.
    this.lock.show();
  }

  public authenticated() {
    // Check if there's an unexpired JWT
    // This searches for an item in localStorage with key == 'id_token'
    return tokenNotExpired();
  }

  public isAdmin(): Promise<boolean> {
    if (!this.authenticated()) {
      return Promise.resolve(false);
    }
    return this.authHttp.get('/api/materials/isAdmin')
      .toPromise()
      .then(response => response.json() as boolean);
  }

  public logout() {
    // Remove token and profile from localStorage
    localStorage.removeItem('id_token');
    Cookie.delete('id_token', '/');
    localStorage.removeItem('profile');
    Cookie.delete('profile', '/');
    this.userProfile = undefined;

    this.store.dispatch(this.signOutEvent());

    if (document.location.pathname.indexOf('/home') === 0) {
      return this.router.navigateByUrl('/');
    }
  }
}
