export interface AuthService {
  // signUp(activationLink:boolean);
  login();
  logout();
  authenticated();
  isAdmin();
}
