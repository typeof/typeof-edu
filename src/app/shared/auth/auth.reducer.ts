import { isBrowser, isNode } from 'angular2-universal'
import { Action } from '@ngrx/store';

import { ClientAuthService } from './client-auth.service';

export interface AuthState {
  authed: boolean;
};

let init: any;

if (isBrowser && window['UNIVERSAL_CACHE'] && window['UNIVERSAL_CACHE']['store']) {
  try {
    init = JSON.parse(window['UNIVERSAL_CACHE']['store']);
    // console.log(init);
  } catch (e) {
    console.log(e);
  }
}

export const initialState: AuthState = init ? init : {
  authed: false,
};

export function authReducer(state = initialState, action: Action): AuthState {
  switch (action.type) {

    case ClientAuthService.AUTH_SIGN_IN: {
      return {
        authed: true,
      };
    }
    case ClientAuthService.AUTH_SIGN_OUT: {
      return {
        authed: false,
      };
    }

    default: {
      return state;
    }
  }
}
