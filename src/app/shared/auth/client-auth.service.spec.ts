/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ClientAuthService } from './client-auth.service';

describe('ClientAuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClientAuthService]
    });
  });

  it('should ...', inject([ClientAuthService], (service: ClientAuthService) => {
    expect(service).toBeTruthy();
  }));
});
