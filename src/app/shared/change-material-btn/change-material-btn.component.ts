import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';

import {
  Status,
  Section,
  SectionsService,
  Material,
  MaterialsService,
  MaterialStatusType,
  MaterialChangeEvent,
} from '../index';
import { ClientAuthService } from '../auth/client-auth.service';

@Component({
  selector: 'app-change-material-btn',
  templateUrl: './change-material-btn.component.html',
  styleUrls: ['./change-material-btn.component.scss']
})
export class ChangeMaterialBtnComponent implements OnInit {

  @Input() material: Material;
  @Input() sections: Section[] = [];
  @Input() status: MaterialStatusType = MaterialStatusType.backlog;
  @Input() addTitle: string = 'Добавить в библиотеку';
  @Input() removeTitle: string = 'Удалить из библиотеки';
  @Input() addColor: string = 'rgba(62, 72, 109, 0.8)';
  @Input() removeColor: string = '#e57373';
  // TODO: transfer this logic into store
  @Output() onChanged = new EventEmitter<MaterialChangeEvent>();

  private change: boolean = false;
  private materialType = MaterialStatusType;

  constructor(
    private authService: ClientAuthService,
    private materialsService: MaterialsService,
    private sectionsService: SectionsService,
  ) { }

  ngOnInit() {
  }

  addToUser(): void {

    this.materialsService.addMaterialToUser(this.material, this.status)
      .then((status: Status<null>) => {
        if (status.success) {
          this.onChanged.emit({
            action: 'added',
            material: this.material,
          });
          this.material.added = true;
          // this.materialStatus.data.added = true;
          // this.error = '';
        } else {
          // this.error = status.msg;
        }
      })
  }

  removeFromUser(): void {
    this.materialsService.removeMaterialFromUser(this.material)
      .then((status: Status<null>) => {
        if (status.success) {
          this.onChanged.emit({
            action: 'removed',
            material: this.material,
          });
          this.material.added = false;
          // this.materialStatus.data.added = false;
          // this.error = '';
        } else {
          // this.onChanged.emit('error');
          // this.error = status.msg;
        }
      });
  }

  changeStatus(newStatus: MaterialStatusType) {
    this.materialsService.changeStatus(this.material, newStatus)
      .then((status: Status<null>) => {
        if (status.success) {
          this.onChanged.emit({
            action: 'status',
            status: newStatus,
            material: this.material,
          })
        }
      });
  }

  addToSection(section: Section) {
    this.sectionsService.addMaterial(this.material, section);
  }

  showShortChange() {
    return (
      this.material &&
      this.authService.authenticated() &&
      this.material.added
      // &&
      // !this.change
    );
  }

  showFullChange() {
    return (
      this.material &&
      this.authService.authenticated() &&
      this.material.added &&
      this.change
    );
  }

  toggleChange() {
    this.change = !this.change;
  }

  getColor(status: MaterialStatusType): string {
    return this.material.status === status ? this.addColor : this.removeColor;
  }

}
