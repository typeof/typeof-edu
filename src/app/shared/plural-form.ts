export const pluralForm = (count: number, forms: string[]) => {

  let res = `${count} `;

  const countBy100 = count % 100;
  const countBy10 = countBy100 % 10;
  if (countBy100 >= 10 && countBy100 <= 20) {
    res += forms[2];
  } else if (countBy10 === 1) {
    res += forms[0];
  } else if (countBy10 >= 2 && countBy10 <= 4) {
    res += forms[1];
  } else {
    res += forms[2];
  }

  return res;
};
