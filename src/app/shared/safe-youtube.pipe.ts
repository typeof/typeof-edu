import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Pipe({
  name: 'safeYoutube'
})
export class SafeYoutubePipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) {}

  transform(videoId: string): SafeResourceUrl {

    if (videoId.indexOf('youtube') !== -1) {
      const id = videoId.replace(/.*watch\?v=(.*)/, '$1');
      return this.sanitizer.bypassSecurityTrustResourceUrl(
        `https://www.youtube-nocookie.com/embed/${id}`
      );
    } else if (videoId.indexOf('youtu.be') !== -1) {
      const id = videoId.replace(/.*youtu.be\/(.*)/, '$1');
      return this.sanitizer.bypassSecurityTrustResourceUrl(
        `https://www.youtube-nocookie.com/embed/${id}`
      );
    }

    return this.sanitizer.bypassSecurityTrustResourceUrl(
      `https://www.youtube-nocookie.com/embed/${videoId}`
    );
  }

}
