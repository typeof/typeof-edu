import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
// import { SafePipe } from './safe.pipe';
import { ChangeMaterialBtnComponent } from './change-material-btn/change-material-btn.component';
import { SecondNavService } from "./second-nav/second-nav.service";
import { SectionsService } from "./sections/sections.service";
import { CategoryLiveTagsComponent } from './category-live-tags/category-live-tags.component';

import {
  MaterialsListComponent,
  MaterialsSidebarComponent,
  MaterialBlockComponent,
} from '.';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    InfiniteScrollModule,
  ],
  declarations: [
    MaterialsListComponent,
    MaterialsSidebarComponent,
    MaterialBlockComponent,
    ChangeMaterialBtnComponent,
    CategoryLiveTagsComponent
  ],
  providers: [
    SecondNavService,
    SectionsService,
  ],
  exports: [
    MaterialsListComponent,
    MaterialsSidebarComponent,
    MaterialBlockComponent,
    ChangeMaterialBtnComponent,
    CategoryLiveTagsComponent,
  ],
})
export class SharedModule {
  // static forRoot() {
  //   return {
  //     ngModule: SharedModule,
  //     providers: [],
  //   };
  // }
}
