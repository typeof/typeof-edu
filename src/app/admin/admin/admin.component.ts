import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
// import { MultiselectDropdownModule } from '../../../../node_modules/angular-2-dropdown-multiselect/src/multiselect-dropdown';
// import { IMultiSelectOption } from '../../../../node_modules/angular-2-dropdown-multiselect/src/multiselect-dropdown';
// import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';
// import { IDropdownItem } from 'ng2-dropdown-multiselect';
import { AppState } from '../../reducers';

import { AdminService } from '../shared/admin.service';

import {
  Material,
  Status,
  MaterialsService,
  CategoriesService,
  CollectionsService,
  Collection,
  CollectionBlock,
  Category,
  Categories,
} from '../../shared';

import { SecondNavService } from '../../shared/second-nav/second-nav.service';

@Component({
  selector: 'app-categories',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  newMaterial: Material = new Material();
  newCategory: Category = new Category();
  newCollection: Collection = new Collection();
  newCollectionBlock: CollectionBlock = new CollectionBlock();

  newRowText1: string = '';
  newRowText2: string = '';
  newRowMedia: string = '';

  showMaterials: boolean = false;

  newRowAuthor: string = '';
  newRowAuthorsDescription: string = '';
  newRowAuthorsMedia: string = '';
  newRowTag: string = '';
  newRowStructure: string = '';

  CatogotyTypes = ['theme', 'platform'];
  MaterialTypes = [
    'book',
    'video',
    'course',
    'article',
  ];
  CollectionBlocksTypes = [
    'cover',
    'author',
    'cause',
    'target',
    'materials',
    'material',
    'materials_short',
    'quote',
    'sources',
    'recommendations',
    'video',
    'video_short',
    'articles',
    'course',
  ];

  catogoryOptions: any[] = [];

  private categoriesStatus: Status<Category[]>;
  private materialsStatus: Status<Material[]>;
  private collectionsStatus: Status<Collection[]>;

  constructor(
    private _categoriesService: CategoriesService,
    private _collectionsService: CollectionsService,
    private _materialsService: MaterialsService,
    private _adminService: AdminService,
    private secondNavService: SecondNavService,
    private store: Store<AppState>
  ) { }

  ngOnInit() {

    this.store.dispatch(this.secondNavService.changeNav({
      title: 'adminochka',
      titleIcon: 'cog',
    }));

    this._categoriesService.getAllCategories(false)
      .then((status: Status<Category[]>) =>
        this.categoriesStatus = status
      );
    this._collectionsService.getAllCollections()
      .then((status: Status<Collection[]>) =>
        this.collectionsStatus = status
      );
    this._materialsService.getAllMaterials()
      .then((status: Status<Material[]>) =>
        this.materialsStatus = status
      );
  }

  toggleMaterial(event, material: Material) {
    if (event.target.checked) {
      this.newCollectionBlock.materials_ids = [
        ...this.newCollectionBlock.materials_ids,
        material.id,
      ];
    } else {
      this.newCollectionBlock.materials_ids = this.newCollectionBlock.materials_ids.filter(id =>
        id !== material.id
      );
    }
  }

  toggleCategory(event, category: Category) {
    if (event.target.checked) {
      this.newMaterial.categories_ids = [
        ...this.newMaterial.categories_ids,
        category.id,
      ];
    } else {
      this.newMaterial.categories_ids = this.newMaterial.categories_ids.filter(id =>
        id !== category.id
      );
    }
  }

  toggleCollection(event, collection: Collection) {
    if (event.target.checked) {
      this.newMaterial.collections_ids = [
        ...this.newMaterial.collections_ids,
        collection.id,
      ];
    } else {
      this.newMaterial.collections_ids = this.newMaterial.collections_ids.filter(id =>
        id !== collection.id
      );
    }
  }

  toggleMaterialAttachement(event, material: Material) {
    if (event.target.checked) {
      this.newMaterial.materials_ids = [
        ...this.newMaterial.materials_ids,
        material.id,
      ];
    } else {
      this.newMaterial.materials_ids = this.newMaterial.materials_ids.filter(id =>
        id !== material.id
      );
    }
  }

  // toggleCollection(event, collection: Collection) {
  //   if (event.target.checked) {
  //     this.newCollectionBlock.categories = [
  //     this.newMaterial.categories = [
  //       ...this.newMaterial.categories,
  //       collection.id,
  //     ];
  //   } else {
  //     this.newMaterial.categories = this.newMaterial.categories.filter(newCategory =>
  //       newCategory !== collection.id
  //     );
  //   }
  // }

  addCategory() {
    this._adminService.addCategory(this.newCategory);
  }

  addMaterial() {
    this._adminService.addMaterial(this.newMaterial);
  }

  addCollection() {
    this._adminService.addCollection(this.newCollection);
  }

  addCollectionBlock() {
    this._adminService.addCollectionBlock(this.newCollectionBlock);
    this.newCollectionBlock = new CollectionBlock();
  }

  addRowTexts1() {
    this.newCollectionBlock.texts_1 = [
      ...this.newCollectionBlock.texts_1,
      this.newRowText1,
    ];
    this.newRowText1 = '';
  }

  addRowTexts2() {
    this.newCollectionBlock.texts_2 = [
      ...this.newCollectionBlock.texts_2,
      this.newRowText2,
    ];
    this.newRowText2 = '';
  }

  addRowMedias() {
    this.newCollectionBlock.medias = [
      ...this.newCollectionBlock.medias,
      this.newRowMedia,
    ];
    this.newRowMedia = '';
  }

  addRowAuthor() {
    this.newMaterial.authors = [
      ...this.newMaterial.authors,
      this.newRowAuthor,
    ];
    this.newRowAuthor = '';
  }

  addRowAuthorsDescription() {
    this.newMaterial.authors_description = [
      ...this.newMaterial.authors_description,
      this.newRowAuthorsDescription,
    ];
    this.newRowAuthorsDescription = '';
  }

  addRowAuthorsMedia() {
    this.newMaterial.authors_media = [
      ...this.newMaterial.authors_media,
      this.newRowAuthorsMedia,
    ];
    this.newRowAuthorsMedia = '';
  }

  addRowTag() {
    this.newMaterial.tags = [
      ...this.newMaterial.tags,
      this.newRowTag,
    ];
    this.newRowTag = '';
  }

  addRowStructure() {
    this.newMaterial.structure = [
      ...this.newMaterial.structure,
      this.newRowStructure,
    ];
    this.newRowStructure = '';
  }
}
