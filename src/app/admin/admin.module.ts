import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

// import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
// import { DropdownMultiselectModule } from 'ng2-dropdown-multiselect';
// import { SelectModule } from 'ng2-select';

import { AdminComponent } from './admin/admin.component';

import { AdminService } from './shared/admin.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    // SelectModule,
    // DropdownMultiselectModule,
    // MultiselectDropdownModule,
  ],
  declarations: [AdminComponent],
  providers: [AdminService]
})
export class AdminModule { }
