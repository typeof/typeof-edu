import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { AuthHttp }      from 'angular2-jwt';

import {
  Status,
  CacheService,
  Category, Categories,
  Collection, CollectionBlock,
  Material,
} from '../../shared';

@Injectable()
export class AdminService {

  private collectionsUrl = '/api/collections';
  private categoriesUrl = '/api/categories';
  private materialsUrl = '/api/materials';

  constructor(
    private authHttp: AuthHttp,
    private http: Http
  ) { }

  addCollection(collection: Collection) {
    return this.authHttp.post(this.collectionsUrl, collection)
      .toPromise()
      .then(res => console.log(res))
      .catch(err => console.log(err));
  }

  addCollectionBlock(collectionBlock: CollectionBlock) {
    return this.authHttp.post(`${this.collectionsUrl}/addCollectionBlock`, collectionBlock)
      .toPromise()
      .then(res => console.log(res))
      .catch(err => console.log(err));
  }

  addCategory(category: Category) {
    return this.authHttp.post(this.categoriesUrl, category)
      .toPromise()
      .then(res => console.log(res))
      .catch(err => console.log(err));
  }

  addMaterial(material: Material) {
    return this.authHttp.post(this.materialsUrl, material)
      .toPromise()
      .then(res => console.log(res))
      .catch(err => console.log(err));
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
