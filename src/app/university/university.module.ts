import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MomentModule } from 'angular2-moment';
import 'moment/locale/ru';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { SharedModule } from '../shared/shared.module';
import { UniversityRoutingModule } from './university-routing.module';

// import { CategoriesService } from './shared/categories.service';
import {
  CollectionsService,
  CategoriesService,
  MaterialsService,
  // MaterialsListComponent,
  // MaterialsSidebarComponent,
  // MaterialBlockComponent,
} from '../shared';

import { CategoriesListComponent } from './categories-list/categories-list.component';
import { CollectionComponent } from './collection/collection.component';
import { CollectionsListComponent } from './collections-list/collections-list.component';
import { MaterialComponent } from './material/material.component';

import { SafeYoutubePipe } from '../shared/safe-youtube.pipe';
import { SubmediaRowComponent } from './material/submedia-row/submedia-row.component';
import { CategoryCatalogComponent } from './category-catalog/category-catalog.component';
import { CategoryBlockComponent } from './categories-list/category-block/category-block.component';
import { CollectionsListBlockComponent } from './collections-list/collections-list-block/collections-list-block.component';
import { CollectionBlockComponent } from './collection/collection-block/collection-block.component';
import { CategoryLiveComponent } from './category-live/category-live.component';
import { CategoryLiveBlockComponent } from './category-live/category-live-block/category-live-block.component';
import { AddedMaterialsComponent } from './added-materials/added-materials.component';
import { EmptyMaterialsComponent } from './empty-materials/empty-materials.component';

@NgModule({
  imports: [
    CommonModule,
    // RouterModule,
    UniversityRoutingModule,
    SharedModule,
    MomentModule,
    InfiniteScrollModule,
  ],
  declarations: [
    SafeYoutubePipe,
    // ChangeMaterialBtnComponent,

    CategoriesListComponent,
    CollectionComponent,
    CollectionsListComponent,
    MaterialComponent,
    SubmediaRowComponent,
    CategoryCatalogComponent,
    CategoryBlockComponent,
    CollectionsListBlockComponent,
    CollectionBlockComponent,
    CategoryLiveComponent,
    CategoryLiveBlockComponent,
    AddedMaterialsComponent,
    EmptyMaterialsComponent,
  ],
  providers: [
    CategoriesService,
    MaterialsService,
    CollectionsService,
  ],
})
export class UniversityModule { }
