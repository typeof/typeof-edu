import { isBrowser } from 'angular2-universal';
import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Store } from '@ngrx/store';

import 'rxjs/add/operator/switchMap';

import { Material } from './../../shared/materials/material.model';
import { AppState } from '../../reducers';

import {
  Status,
  Collection, CollectionsService,
} from '../../shared';

import { SecondNavService } from '../../shared/second-nav/second-nav.service';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionComponent implements OnInit {

  private collectionStatus: Status<Collection>;
  private back = {
    btn: true,
    url: '/library/collections',
    title: 'Подборки',
    icon: 'puzzle-piece',
  };

  constructor(
    private _route: ActivatedRoute,
    private _collectionsService: CollectionsService,
    private secondNavService: SecondNavService,
    private titleService: Title,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    this.store.dispatch(this.secondNavService.changeNav({
      back: this.back,
    }));

    this._route.params
      .switchMap((params: Params) =>
        this._collectionsService.getCollectionByAlias(params['alias'])
      )
      .subscribe((collectionStatus: Status<Collection>) => {
        this.collectionStatus = collectionStatus

        if (isBrowser) {
          this.titleService.setTitle(`${this.collectionStatus.data.title} | You Know`);
        }
        // if (materialStatus && materialStatus.data && materialStatus.data.type) {
          // this.title = Promise.resolve(materialStatus.data.type);
        // }
      });
  }

}
