import { Material } from './../../../shared/materials/material.model';
import { CollectionBlock, CollectionBlockType } from './../../../shared/collections/collection.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-collection-block',
  templateUrl: './collection-block.component.html',
  styleUrls: ['./collection-block.component.scss']
})
export class CollectionBlockComponent implements OnInit {

  @Input() block: CollectionBlock;

  collectionBlockType = CollectionBlockType;

  constructor() { }

  ngOnInit() {
    if (this.block.materials) {
      this.block.materials = this.block.materials
        .map(material => new Material(material))
        .map(material => {
          let tmp = material.description.match(/(.{100,300})\. (.*)/m);
          if (tmp && tmp.length > 1) {
            material.description = tmp[1] + '...';
          }

          return material;
        });
    }
  }

  scrollToTop(scrollDuration) {
    window.scrollTo(0, 0);
  }
}
