import { SecondNavComponent } from './../../shared/second-nav/second-nav.component';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Store } from '@ngrx/store';

import 'rxjs/add/operator/switchMap';

import { AppState } from '../../reducers';
import {
  Status,
  Material,
  MaterialStatusType,
  Category,
} from '../../shared/';
import { CategoriesService } from '../../shared/categories/categories.service';
import { MaterialsService } from '../../shared/materials/materials.service';
import { SecondNavService } from '../../shared/second-nav/second-nav.service';

@Component({
  selector: 'app-category-catalog',
  templateUrl: './category-catalog.component.html',
  styleUrls: ['./category-catalog.component.scss']
})
export class CategoryCatalogComponent implements OnInit {

  private materialsPromise: Promise<Status<Material[]>>;
  private materialsStatus: Status<Material[]>;
  private categoryStatus: Status<Category>;
  private category: Category;
  private sortContentByRating: boolean = true;
  private error: string;
  private byPlatform = false;
  private back = {
    btn: true,
    url: '/library/categories',
    title: 'Направления',
    icon: 'university',
  };

  constructor(
    private route: ActivatedRoute,
    private categoriesService: CategoriesService,
    private materialsService: MaterialsService,
    private secondNavService: SecondNavService,
    private store: Store<AppState>
  ) { }

  ngOnInit() {

    this.store.dispatch(this.secondNavService.changeNav({
      back: this.back,
    }));

    if (this.route.snapshot.url[1].path === 'platforms') {
      this.byPlatform = true;
    }

    this.store.select('auth').subscribe(() => 
      this.init()
    );

    this.init();
  }

  init(sortContentByRating: boolean = true) {
    const alias = this.route.snapshot.params['alias'];

    this.categoriesService.getCategory(alias, this.byPlatform)
      .then(status => {
        if (!status.success) {
          return this.error = status.msg;
        }

        this.category = status.data;
        this.store.dispatch(this.secondNavService.changeNav({
          back: this.back,
          tabs: [{
            url: `/library/${this.byPlatform ? 'platforms' : 'categories'}/${alias}`,
            title: status.data.title,
            active: false,
          }, {
            url: `/library/${this.byPlatform ? 'platforms' : 'categories'}/${alias}/catalog`,
            title: 'Каталог',
            active: true,
            icon: 'th',
          }],
        }));

        this.materialsPromise = this.materialsService
          .getMaterialsByCategory(this.category, 0, 0, sortContentByRating, this.byPlatform);
      });
  }

  loadContent(sortContentByRating: boolean) {
    if (
      !(sortContentByRating && this.sortContentByRating ||
        !sortContentByRating && !this.sortContentByRating)
    ) {
      this.init(sortContentByRating);
      this.sortContentByRating = !this.sortContentByRating;
    }
  }

}
