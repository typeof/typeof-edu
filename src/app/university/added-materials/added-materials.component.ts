import { SecondNavService } from './../../shared/second-nav/second-nav.service';
import { AppState } from './../../reducers/index';
import { Store } from '@ngrx/store';
import { Category } from './../../shared/categories/category.model';
import { Component, OnInit } from '@angular/core';

import { MaterialsService } from './../../shared/materials/materials.service';
import { Material } from './../../shared/materials/material.model';
import { Status } from './../../shared/status.model';

@Component({
  selector: 'app-added-materials',
  templateUrl: './added-materials.component.html',
  styleUrls: ['./added-materials.component.scss']
})
export class AddedMaterialsComponent implements OnInit {

  private materialsStatus: Status<Material[]>;
  private materialsPromise: Promise<Status<Material[]>>;
  private category: Category = new Category();

  constructor(
    private materialsService: MaterialsService,
    private secondNavService: SecondNavService,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    this.category.title = 'Мои материалы';
    this.init();
  }

  init() {

    this.materialsPromise = this.materialsService
      .getMaterialsByUser();

    this.store.dispatch(this.secondNavService.changeNav({
      tabs: [{
        url: `/my-materials`,
        title: 'Мои материалы',
        active: false,
        icon: 'bookmark-o',
      }, {
        url: `/my-materials/catalog`,
        title: 'Каталог',
        active: true,
        icon: 'th',
      }],
    }));
  }
}
