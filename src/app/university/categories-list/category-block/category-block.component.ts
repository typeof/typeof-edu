import { Component, OnInit, Input } from '@angular/core';

import { Category } from './../../../shared/categories/category.model';
@Component({
  selector: 'app-category-block',
  templateUrl: './category-block.component.html',
  styleUrls: ['./category-block.component.scss']
})
export class CategoryBlockComponent implements OnInit {

  @Input() category: Category;
  @Input() short: boolean = false;
  @Input() isCollection: boolean = false;
  
  private description: string = '';
  private url: string = '';

  constructor() { }

  ngOnInit() {
    if (this.isCollection) {
      this.url = `/library/collections/${this.category.alias}`
    } else {
      this.url = `/library/${this.category.type == 1 ? 'categories' : 'platforms'}/${this.category.alias}`;
    }

    this.description = this.category.description.replace(/(.{50,150})\. (.*)/, '$1...')
    if (this.description.length > 200) {
      this.description = this.category.description.replace(/(.{50,150})[\., ](.*)/, '$1...')
    }
  }

}
