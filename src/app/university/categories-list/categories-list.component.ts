import { isBrowser } from 'angular2-universal';
import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
// import { Component } from '@angular/core';
import { AppState } from '../../reducers';
import { Store } from '@ngrx/store';
import { go, replace, search, show, back, forward } from '@ngrx/router-store';
import { NavActions } from '../../core/nav/nav.actions';
import { SecondNavService } from '../../shared/second-nav/second-nav.service';
import { pluralForm } from '../../shared/plural-form';

import {
  Status,
  Category, Categories, CategoriesService,
} from '../../shared';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss']
})
export class CategoriesListComponent implements OnInit {

  tabs = [{
    url: '/library/categories',
    title: 'Направления',
    active: true,
    icon: 'university',
  },{
    url: '/library/collections',
    title: 'Подборки',
    active: false,
    icon: 'puzzle-piece',
  }];

  // private categoriesStatus: Status<Categories>;
  private categoriesByThemes: Status<Category[]>;
  private categoriesByPlatform: Status<Category[]>;
  private themsLength: string;
  private platformsLength: string;

  constructor(
    private categoriesService: CategoriesService,
    private navActions: NavActions,
    private secondNavService: SecondNavService,
    private titleService: Title,
    private store: Store<AppState>
  ) { }

  ngOnInit() {

    if (isBrowser) {
      this.titleService.setTitle('Агрегатор образовательного контента | You Know');
    }

    this.store.dispatch(this.secondNavService.changeNav({
      tabs: this.tabs,
    }));

    this.categoriesService.getAllCategories(false)
      .then((status: Status<Category[]>) => {
        this.categoriesByThemes = status;
        this.themsLength = pluralForm(this.categoriesByThemes.data.length, ['раздел', 'раздела', 'разделов']);
        // this.platformsLength = pluralForm(this.categoriesStatus.data.categoriesByPlatform.length, ['раздел', 'раздела', 'разделов'])
      });

    this.categoriesService.getAllCategories(true)
      .then((status: Status<Category[]>) => {
        this.categoriesByPlatform = status;
        this.platformsLength = pluralForm(this.categoriesByPlatform.data.length, ['раздел', 'раздела', 'разделов']);
        // this.platformsLength = pluralForm(this.categoriesStatus.data.categoriesByPlatform.length, ['раздел', 'раздела', 'разделов'])
      });
    // this.categories = this.categoriesService.getCategories();
    // this.categoriesService.getCategoriesByPlatform()
    //   .then((categoriesByPlatform: Category[]) => {
    //     this.categoriesByPlatform = categoriesByPlatform;
    //   });
  }

  openMaterialsList({ alias, title }) {
    this.store.dispatch(this.navActions.changeNav({
      currentPath: `/categories/${alias}`,
      currentTitle: title,
      prevPath: '/categories',
      prevTitle: 'Направления',
    }));
    this.store.dispatch(go(['/categories', alias]));
  }

}
