import { isBrowser } from 'angular2-universal';
import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState } from '../../reducers';

import {
  Status,
  Collection, CollectionsService,
} from '../../shared/index';

import { SecondNavService } from '../../shared/second-nav/second-nav.service';

@Component({
  selector: 'app-collections-list',
  templateUrl: './collections-list.component.html',
  styleUrls: ['./collections-list.component.scss']
})
export class CollectionsListComponent implements OnInit {

  tabs = [{
    url: '/library/categories',
    title: 'Направления',
    active: false,
    icon: 'university',
  },{
    url: '/library/collections',
    title: 'Подборки',
    active: true,
    icon: 'puzzle-piece',
  }];

  private collectionsStatus: Status<Collection[]>;

  constructor(
    private collectionsService: CollectionsService,
    private secondNavService: SecondNavService,
    private titleService: Title,
    private store: Store<AppState>
  ) { }

  ngOnInit() {

    if (isBrowser) {
      this.titleService.setTitle(`Тематические подборки | You Know`);
    }

    this.store.dispatch(this.secondNavService.changeNav({
      tabs: this.tabs,
    }));

    this.collectionsService.getAllCollections()
      .then((status: Status<Collection[]>) =>
        this.collectionsStatus = status
      );
  }

}
