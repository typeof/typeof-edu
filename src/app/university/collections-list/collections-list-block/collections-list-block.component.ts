import { Component, OnInit, Input } from '@angular/core';

import { Collection } from './../../../shared/collections/collection.model';
import { materialPluralForm } from './../../../shared/materials/material.model';

@Component({
  selector: 'app-collections-list-block',
  templateUrl: './collections-list-block.component.html',
  styleUrls: ['./collections-list-block.component.scss']
})
export class CollectionsListBlockComponent implements OnInit {

  @Input() collection: Collection; 
  @Input() reverse: boolean = false; 

  private getPluralForm = materialPluralForm;

  constructor() { }

  ngOnInit() {
  }

}
