import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoriesListComponent } from './categories-list/categories-list.component';
import { CollectionsListComponent } from './collections-list/collections-list.component';
import { CollectionComponent } from './collection/collection.component';
import { MaterialComponent } from './material/material.component';
import { CategoryCatalogComponent } from './category-catalog/category-catalog.component';
import { CategoryLiveComponent } from './category-live/category-live.component';
import { AddedMaterialsComponent } from './added-materials/added-materials.component';

const routes: Routes = [
  { path: 'library/categories', component: CategoriesListComponent },
  { path: 'library/collections', component: CollectionsListComponent },
  { path: 'my-materials', component: CategoryLiveComponent },
  { path: 'my-materials/catalog', component: AddedMaterialsComponent },
  { path: 'my-materials/catalog/:tag', component: AddedMaterialsComponent },
  { path: 'library/collections/:alias', component: CollectionComponent },
  { path: 'library/categories/:alias', component: CategoryLiveComponent },
  { path: 'library/categories/:alias/catalog', component: CategoryCatalogComponent },
  { path: 'library/categories/:alias/catalog/:tag', component: CategoryCatalogComponent },
  { path: 'library/platforms/:alias', component: CategoryLiveComponent },
  { path: 'library/platforms/:alias/catalog', component: CategoryCatalogComponent },
  { path: 'library/platforms/:alias/catalog/:tag', component: CategoryCatalogComponent },
  { path: 'library/material/:alias', component: MaterialComponent },
  { path: '**',  redirectTo: '' },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class UniversityRoutingModule { }
