import { Title } from '@angular/platform-browser';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { isBrowser } from 'angular2-universal';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Store } from '@ngrx/store';

import 'rxjs/add/operator/switchMap';

import { AppState } from '../../reducers';
import {
  Status,
  Material,
  TinyMaterial,
  MaterialsService,
  MaterialStatusType,
  MaterialChangeEvent,
} from '../../shared/';

import { ClientAuthService } from '../../shared/auth/client-auth.service';
import { SecondNavService } from '../../shared/second-nav/second-nav.service';

@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.scss']
})
export class MaterialComponent implements OnInit, OnDestroy {

  back = {
    btn: true,
    url: '/library/categories',
    title: 'Направления',
    icon: 'university',
  };

  materialStatus: Status<Material>;
  material: Material;

  error: string = '';

  private showAttach: boolean = false;
  private materials: Material[] = [];
  private alias: string;
  private sub: any;
  private isAdmin: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private authService: ClientAuthService,
    private materialsService: MaterialsService,
    private secondNavService: SecondNavService,
    private titleService: Title,
    private store: Store<AppState>
  ) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.alias = params['alias'];

      // this.store.dispatch(this.secondNavService.changeNav({
      //   back: this.back,
      // }));

      // this.init();

      this.store.select('auth').subscribe(() => 
        this.init()
      );

      if (isBrowser) {
        this.authService.isAdmin().then(isAdmin => {
          if (!isAdmin) {
            this.isAdmin = false;
            return;
          }
          this.isAdmin = true;
          this.materialsService.getAllMaterials()
            .then(status => {
              if (status.success) {
                this.materials = status.data;
              }
            });
        })
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  init() {
    const alias = this.alias;
    
    this.materialsService.getMaterial(alias)
      .then(materialStatus =>  {
        this.materialStatus = materialStatus
        if (materialStatus && materialStatus.data && materialStatus.data.type) {
          this.material = new Material(materialStatus.data);

          if (isBrowser) {
            this.titleService.setTitle(`${this.material.title} | You Know`);
          }

          this.store.dispatch(this.secondNavService.changeNav({
            // back: this.back,
            title: this.material.typeName,
          }));
        }
      });
  }

  onChanged({ action, status }: MaterialChangeEvent) {
    if (action === 'added') {
      this.materialStatus.data.added = true;
      this.materialStatus.data.status = MaterialStatusType.backlog;
    } else if (action === 'removed') {
      this.materialStatus.data.added = false;
    } else if (action === 'status') {
      this.materialStatus.data.status = status;
    } else {
      this.error = 'something goes wrong';
    }
  }

  onMaterialChanged({ status, action, material }: MaterialChangeEvent) {
    if (action === 'added') {
      this.material.materials = this.material.materials.map(curMaterial => {
        if (material.id === curMaterial.id) {
          curMaterial.added = true;
          curMaterial.status = MaterialStatusType.backlog;
        }
        return curMaterial;
      });
    } else if (action === 'removed') {
      this.material.materials = this.material.materials.map(curMaterial => {
        if (material.id === curMaterial.id) {
          curMaterial.added = false;
        }
        return curMaterial;
      });
    } else if (action === 'status') {
      this.material.materials = this.material.materials.map(curMaterial => {
        if (material.id === curMaterial.id) {
          curMaterial.status = status;
        }
        return curMaterial;
      });
    } else {
      this.error = 'something goes wrong';
    }
  }

  toggleShowAttach() {
    this.showAttach = !this.showAttach;
  }

  toggleAttachment(materialInner: TinyMaterial) {
    const attached = this.material.materials.filter((material) => 
      material.id === materialInner.id
    ).length;

    if (attached) {
      return this.materialsService.removeAttachement(this.material, materialInner)
        .then(() => {
          this.init();
        });
    }
    return this.materialsService.addAttachement(this.material, materialInner)
      .then(() => {
        this.init();
      });
  }
}
