import { Component, OnInit, Input } from '@angular/core';

import { Material } from '../../../shared';

@Component({
  selector: 'app-submedia-row',
  templateUrl: './submedia-row.component.html',
  styleUrls: ['./submedia-row.component.scss']
})
export class SubmediaRowComponent implements OnInit {

  @Input() course: boolean = false;
  @Input() value: string = '';
  @Input() name: string = '';
  @Input() icon: string = '';

  constructor() { }

  ngOnInit() {
    if (this.name === 'Длительность' && this.value) {
      let res = '';
      if (+this.value >= 60) {
        res = `${(+this.value/60).toFixed()}ч. `;
      }

      res += `${+this.value%60}м.`
      this.value = res;
    }
  }

}
