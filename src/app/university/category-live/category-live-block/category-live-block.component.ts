import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Material, MaterialType } from './../../../shared/materials/material.model';

@Component({
  selector: 'app-category-live-block',
  templateUrl: './category-live-block.component.html',
  styleUrls: ['./category-live-block.component.scss']
})
export class CategoryLiveBlockComponent implements OnInit {

  @Input() material: Material;
  @Output() onMaterialStatusChanged = new EventEmitter<any>();
  private description: string = '';

  constructor() { }

  ngOnInit() {
    let tmp = this.material.description.match(/(.{1,100})\. (.*)/m);
    if (tmp && tmp.length > 1) {
      this.description = tmp[1] + '...';
    }
    if (this.description.length > 99) {
      tmp = this.material.description.match(/(.{50,100})[\., ](.*)/m);

      if (tmp && tmp.length > 1) {
        this.description = tmp[1] + '...';
      }
    }
  }

}
