import { isBrowser } from 'angular2-universal';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Store } from '@ngrx/store';
import * as _ from 'underscore';

import { AppState } from '../../reducers';

import {
  Status,
  Material,
  MaterialStatusType,
  Category,
  tagsWhiteList,
} from '../../shared/';

import { materialPluralForm } from './../../shared/materials/material.model';

import { ClientAuthService } from './../../shared/auth/client-auth.service';
import { SecondNavService } from '../../shared/second-nav/second-nav.service';
import { MaterialsService } from './../../shared/materials/materials.service';
import { CategoriesService } from '../../shared/categories/categories.service';

@Component({
  selector: 'app-category-live',
  templateUrl: './category-live.component.html',
  styleUrls: ['./category-live.component.scss']
})
export class CategoryLiveComponent implements OnInit {

  private materialsStatus: Status<Material[]>;
  private filteredMaterials: Material[] = [];
  private cuttedMaterials: Material[] = [];
  private categoryStatus: Status<Category>;
  private category: Category = new Category();
  private error: string;
  private sortContentByRating: boolean = true;
  private savedMaterialsPage: boolean = false;
  private pageLength: number = 10;
  private infoPage: boolean = false;
  private tags: {tag: string, count: number}[] = [];
  private platforms: {tag: string, count: number}[] = [];
  private showAll = false;
  private byPlatform = false;
  private tabs = null;
  private getPluralForm = materialPluralForm;
  private back = {
    btn: true,
    url: '/library/categories',
    title: 'Направления',
    icon: 'university',
  };

  constructor(
    private authService: ClientAuthService,
    private route: ActivatedRoute,
    private materialsService: MaterialsService,
    private categoriesService: CategoriesService,
    private secondNavService: SecondNavService,
    private titleService: Title,
    private store: Store<AppState>
  ) { }

  ngOnInit() {

    this.store.select('auth').subscribe(() => 
      this.init()
    );

    this.init();
  }

  init(changeSorting: boolean = false) {

    if (this.route.snapshot.url[0].path === 'my-materials') {
      this.savedMaterialsPage = true;
      this.initForSavedMaterials();
      if (isBrowser) {
        this.titleService.setTitle('Мои материалы | You Know');
      }
      return;
    }

    if (this.route.snapshot.url[1].path === 'platforms') {
      this.byPlatform = true;
    }

    this.store.dispatch(this.secondNavService.changeNav({
      back: this.back,
    }));

    const alias = this.route.snapshot.params['alias'];

    this.categoriesService.getCategory(alias, this.byPlatform)
      .then(status => {
        if (!status.success) {
          return this.error = status.msg;
        }

        this.category = status.data;

        if (isBrowser) {
          this.titleService.setTitle(`${this.category.title} | You Know`);
        }

        this.tabs = [{
            url: `/library/${this.byPlatform ? 'platforms' : 'categories'}/${alias}`,
            title: status.data.title,
            active: true,
            // icon: 'coffee',
          }, {
            url: `/library/${this.byPlatform ? 'platforms' : 'categories'}/${alias}/catalog`,
            title: 'Каталог',
            active: false,
            icon: 'th',
          }];
        this.store.dispatch(this.secondNavService.changeNav({
          back: this.back,
          // title: status.data.title,
          tabs: this.tabs,
        }));

        if (changeSorting) {
          this.sortContentByRating = !this.sortContentByRating;
        }

        this.materialsService
          .getMaterialsByCategory(this.category, 0, 0, this.sortContentByRating, this.byPlatform)
          .then(status => {
            // this.filteredMaterials = status.data || [];
            this.materialsStatus = status;
            this.collectCategoryTags();
            this.filterMaterials();
          });
      });
  }

  initForSavedMaterials() {
    this.tabs = [{
      url: `/my-materials`,
      title: 'Мои материалы',
      active: true,
      icon: 'bookmark-o',
    }];
    this.store.dispatch(this.secondNavService.changeNav({
      // back: this.back,
      // title: status.data.title,
      tabs: this.tabs,
    }));

    if (this.authService.authenticated()) {
      this.infoPage = false;
      this.materialsService
        .getMaterialsByUser()
        .then(status => {

          this.materialsStatus = status;

          if (!status.success) {
            this.error = status.msg;
            return;
          }

          if (status.success && status.data.length) {
            this.tabs = [{
              url: `/my-materials`,
              title: 'Мои материалы',
              active: true,
              icon: 'bookmark-o',
            }, {
              url: `/my-materials/catalog`,
              title: 'Каталог',
              active: false,
              icon: 'th',
            }];
            this.store.dispatch(this.secondNavService.changeNav({
              // back: this.back,
              // title: status.data.title,
              tabs: this.tabs,
            }));
          }

          this.collectCategoryTags();
          this.filterMaterials();
        })
    } else {
      this.infoPage = true;
    }
  }

  filterMaterials() {
    this.pageLength = 10;

    this.filteredMaterials = this.materialsStatus.data
      .map(material => new Material(material))
      // .filter(material => this.materialFilters[material.typeName])
      // .filter(material => {
      //   if ((this.assignation === AssignationType.Assigned && !material.added) ||
      //     (this.assignation === AssignationType.NotAssigned && material.added)) {
      //     return false;
      //   }
      //   return true ;
      // });

    // if (this.authService.authenticated()) {
    //   this.filteredMaterials = this.filteredMaterials
    //     .filter(material =>
    //       material.added ? this.materialFilters[material.statusName] : true
    //     );
    // }

    this.category.books = this.filteredMaterials.filter(mat => mat.book).length;
    this.category.videos = this.filteredMaterials.filter(mat => mat.video).length;
    this.category.courses = this.filteredMaterials.filter(mat => mat.course).length;
    this.category.articles = this.filteredMaterials.filter(mat => mat.article).length;

    this.sliceMaterials();
  }

  flatMap<T, U>(array: T[], callbackfn: (value: T, index: number, array: T[]) => U[]): U[] {
    return [].concat(...array.map(callbackfn));
  }

  collectCategoryTags() {

    const flatTags = this.flatMap(
      this.materialsStatus.data,
      material => material.tags
    );

    const tagsObj = _.countBy(flatTags, (tag) => tag);

    this.tags = Object.keys(tagsObj)
      .map(tag => ({
        tag,
        count: tagsObj[tag],
      }))
      .filter(tag =>
        tagsWhiteList.indexOf(tag.tag) !== -1
      )
      .sort((a, b) =>
        b.count - a.count
      );
    
    const flatPlatforms = this.materialsStatus.data.map(material => material.platform);

    const platformsObj = _.countBy(flatPlatforms, (platform) => platform);

    this.platforms = Object.keys(platformsObj)
      .map(platform => ({
        tag: platform,
        count: platformsObj[platform],
      }))
      .filter(tag => tag.tag !== 'null')
      .sort((a, b) =>
        b.count - a.count
      );
  }

  sliceMaterials() {
    this.cuttedMaterials = this.filteredMaterials.slice(0, this.pageLength);
  }

  loadContent(sortContentByRating: boolean) {
    if (
      !(sortContentByRating && this.sortContentByRating ||
        !sortContentByRating && !this.sortContentByRating)
    ) {
      this.init(true);
    }
  }

  onScroll() {
    this.pageLength += 10;
    this.sliceMaterials();
  }

}
