import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-empty-materials',
  templateUrl: './empty-materials.component.html',
  styleUrls: ['./empty-materials.component.scss']
})
export class EmptyMaterialsComponent implements OnInit {

  @Input() notAuthed: boolean = false;
  constructor() { }

  ngOnInit() {
  }

}
