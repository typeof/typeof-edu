/**
 * the polyfills must be the first thing imported
 */
import './polyfills.ts';
import './__2.1.1.workaround.ts'; // temporary until 2.1.1 things are patched in Core
import * as path from 'path';
import * as express from 'express';
import * as compression from 'compression';
import * as cookieParser from 'cookie-parser';
import { json, urlencoded } from 'body-parser';
import { createEngine } from 'angular2-express-engine';
import { enableProdMode } from '@angular/core';
import { AppModule } from './app/app.node.module';
import { environment } from './environments/environment';
import { routes } from './server.routes';

import { collectionsRouter } from './server/api/collections';
import { categoriesRouter } from './server/api/categories';
import { materialsRouter } from './server/api/materials';
import { sectionsRouter } from './server/api/sections';

// App

const app  = express();
const ROOT = path.join(path.resolve(__dirname, '..'));
// const port = 443;
const port = process.env.PORT || environment.port || 4200;

/**
 * enable prod mode for production environments
 */
if (environment.production) {
  enableProdMode();
}

app.disable('x-powered-by');

/**
 * Express View
 */
app.engine('.html', createEngine({}));
app.set('views', path.join(ROOT, 'client'));
app.set('view engine', 'html');

app.use((req,res,next) =>{
  let host = req.get('host');
  if (/^www\./.test(host)) {
    host = host.substring(4, host.length);
    res.writeHead(301, {
      'Location': 'https://' + host + req.originalUrl,
      'Expires': new Date().toUTCString()
    });
    res.end();
  } else if (req.protocol != 'https') {
    res.writeHead(301, {
      'Location': 'https://' + host + req.originalUrl,
      'Expires': new Date().toUTCString()
    });
    res.end();
  } else {
    next();
  }
});

/**
 * Enable compression
 */
app.use(json());
app.use(compression());
app.use(urlencoded({ extended: true }));
app.use(cookieParser());

/**
 * serve static files
 */
app.use('/', express.static(path.join(ROOT, 'client'), {index: false}));

/**
 * place your api routes here
 */
app.use('/api/collections', collectionsRouter);
app.use('/api/categories', categoriesRouter);
app.use('/api/materials', materialsRouter);
app.use('/api/sections', sectionsRouter);

/**
 * bootstrap universal app
 * @param req
 * @param res
 */
function ngApp(req: any, res: any) {
  res.render('index', {
    req,
    res,
    ngModule: AppModule,
    preboot: false,
    baseUrl: '/',
    requestUrl: req.originalUrl,
    // originUrl: req.hostname,
    originUrl: (environment.production ? 'https://' : 'http://') + req.hostname + ':' + port
  });
}

/**
 * use universal for specific routes
 */
app.get('/', ngApp);
app.get('/*', ngApp);
// app.get('/test', (req, res) => {
//   console.log(res.json({ wow: 'wow'}));
//   // console.log(res, req)
// });
routes.forEach(route => {
  app.get(`/${route}`, ngApp);
  app.get(`/${route}/*`, ngApp);
});

/**
 * if you want to use universal for all routes, you can use the '*' wildcard
 */

app.get('*', function (req: any, res: any) {
  res.setHeader('Content-Type', 'application/json');
  const pojo = {status: 404, message: 'No Content'};
  const json = JSON.stringify(pojo, null, 2);
  res.status(404).send(json);
});

import * as http from 'http';
import * as https from 'https';
import * as fs from 'fs';

if (environment.production) {
  const privateKey = fs.readFileSync('/etc/letsencrypt/live/u-know.ru/privkey.pem');
  const certificate = fs.readFileSync('/etc/letsencrypt/live/u-know.ru/cert.pem');
  const ca = fs.readFileSync( '/etc/letsencrypt/live/u-know.ru/fullchain.pem');

  const server = http.createServer(app);
  https.createServer({
    key: privateKey,
    cert: certificate,
    ca,
  }, app).listen('443');

  server.listen(80)

} else {
  app.listen(port, () => {
    console.log(`Listening on port ${port}`);
  });
}

